import { Request, Response } from 'express';
import * as helloService from '../services/hello.services.js';
import { PublicRequest } from 'app-request';
import { SuccessResponse } from '../core/ApiResponse';
import _ from 'lodash';
import path from 'path';


const sayHello = async (req:PublicRequest, res:Response) => {
    const name = req.params.name;
    const message = {message: `Hello ${name}!`};
    // return new SuccessResponse(
    //   'success',
    //   _.pick(message, ['message']),
    // ).send(res);
    // send html file root hihi.html
    const htmlFilePath = `<!DOCTYPE html>
    <html lang="en">
    <head>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <title>Form Submission</title>
        <script>
            async function sendMessage(event) {
                event.preventDefault();
                const message = document.getElementById("messageInput").value;
    
                const response = await fetch("192.168.1.4/hello", {
                    method: "POST",
                    headers: {
                        "Content-Type": "application/json",
                        "x-api-key": "GCMUDiuY5a7WvyUNt9n3QztToSHzK7Uj"
                    },
                    body: JSON.stringify({ message: message })
                });
    
                const result = await response.json();
                console.log(result);
            }
        </script>
    </head>
    <body>
        <form onsubmit="sendMessage(event)">
            <label for="messageInput">Message:</label>
            <input type="text" id="messageInput" name="message" required>
            <button type="submit">Send</button>
        </form>
    </body>
    </html>
    `
    
    return res.send(htmlFilePath);
}

export { sayHello };
