import { Prisma, sizes as Sizes, system_settings as SystemSetting } from '@prisma/client';
import prisma from '../../db/prisma/connection';

async function findByKey(key: string):Promise<SystemSetting|null> {
    return await prisma.system_settings.findFirst({
        where: {
            key: key
        }
    });
}

export default {
    findByKey
}