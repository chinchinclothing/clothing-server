import {Router} from 'express';
import authorization from '../../../auth/authorization';
import authentication from '../../../auth/authentication';
import { e_role_code as RoleCode } from '@prisma/client';
import accessRoles from '../../../helpers/accessRoles'
import validator ,{ValidationSource} from '../../../helpers/validator';
import asyncHandler from '../../../helpers/asyncHandler';
import {ProtectedRequest, PublicRequest} from 'app-request';

import schema from '../schema';
import locationServices from '../../../services/locations';

import { SuccessResponse } from '../../../core/ApiResponse';
import { BadRequestError } from '../../../core/ApiError';
import _ from 'lodash';
//================================================================
const router = Router();
//================================================================
/**
 * 
 * get location by id
 * 
 */
router.get(
    '/',
    validator(schema.locationId, ValidationSource.QUERY),
    asyncHandler(async (req: ProtectedRequest, res) => {
        const location = await locationServices.getLocationById(parseInt(req.query.location_id as string));
        if (!location) throw new BadRequestError('Location not found');
        new SuccessResponse('Location get successfully', {location}).send(res);
    })
)

/**
 * 
 * Get sub locations
 * 
 */

router.get(
    '/sub',
    validator(schema.locationId, ValidationSource.QUERY),
    asyncHandler(async (req: ProtectedRequest, res) => {
        let location_id = parseInt(req.query.location_id as string) as number | null;
        if (location_id === -1){
            location_id = null;
        }
        const sub_locations = await locationServices.getSubLocationById(location_id);
        const location = location_id?await locationServices.getLocationById(location_id as number):null;
        new SuccessResponse('Location get successfully', 
            {
                location: !location?null:_.pick(location, ['id','country', 'name', 'level', 'longitude', 'latitude', 'postcode', 'parent_id']),
                sub_locations:_.map(sub_locations,(sub_location)=>_.pick(sub_location, ['id','country', 'name', 'level', 'longitude', 'latitude', 'postcode']))
            }
        ).send(res);
    })
)

export default router;