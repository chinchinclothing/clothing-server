import express, { Request, Response, NextFunction } from 'express';
import Logger from './core/Logger';
import cors from 'cors';
import { corsUrl, environment, port, portSocket } from './config';
// import './database'; // initialize database
// import './cache'; // initialize cache
import {
  NotFoundError,
  ApiError,
  InternalError,
  ErrorType,
} from './core/ApiError';
import Routes from './routes';
import * as http from 'http'
import path from 'path';
//**========================================================= */
class App {
  private app: express.Application;
  private server: http.Server;
  private configs: any;
    /**
     * 
     * sets the properties to be used by this class to create the server
     * 
     */
  constructor() {
    this.app = express();
    this.server = http.createServer(this.app);

   
    this.configs ={
      get port() {
        return port || 3003;
        },
        
        get portSocket() {
          return portSocket || 3001;
        },

        get accessUrl() {
          return {
            origin: corsUrl,
          }
        },
        
        get environment() {
          return environment || 'development';
        }
      }
      
      this.initializeMiddlewares();
      this.initializeRoutes();
      this.initializeErrorHandling();
      this.handleUncaughtExceptions();
    }
    /**
     * 
     * Applies middleware to be use by this app
     * 
     */
  private initializeMiddlewares() {
    try {
      this.app.use(express.json({ limit: '10mb' }));
      this.app.use(
        express.urlencoded({ limit: '10mb', extended: true, parameterLimit: 50000 }),
      );
      this.app.use(cors({ origin: this.configs?.accessUrl?.origin, optionsSuccessStatus: 200 }));
      this.app.use(express.static(path.join(__dirname, 'public')));
    } catch (error) { 
      console.log("error middlewares",error)
    }

}

  private initializeRoutes() {
    // Routes
    new Routes(this.app);
    // catch 404 and forward to error handler
    this.app.use((req, res, next) => next(new NotFoundError()));
  }

  private initializeErrorHandling() {
    // Middleware Error Handler
    // eslint-disable-next-line @typescript-eslint/no-unused-vars
    this.app.use((err: Error, req: Request, res: Response, next: NextFunction) => {
      if (err instanceof ApiError) {
        ApiError.handle(err, res);
        if (err.type === ErrorType.INTERNAL) {
          console.log(
            `500 - ${err.message} - ${req.originalUrl} - ${req.method} - ${req.ip}`
          );
        }
      } else {
        console.log(
          `500 - ${err.message} - ${req.originalUrl} - ${req.method} - ${req.ip}`
        );
        console.log(err);
        if (environment === 'development') {
          return res.status(500).send(err);
        }
        ApiError.handle(new InternalError(), res);
      }
    });
  }

  private handleUncaughtExceptions() {
    process.on('uncaughtException', (e) => {
      console.log(e);
    });
  }

  public run() {
    this.server.listen(this.configs.port, () => {
      console.log(`Server in "${this.configs.environment}" mode, listening on *:${this.configs.port}`);
      // console.log(`Server listening on *:${port}`);
    });
  }
}

export default App;

// To start the server, you would create an instance of the App class and call the run method.
// const app = new App();
// app.run();
