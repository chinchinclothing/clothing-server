async function sortObject(obj: any) {
    let keys = Object.keys(obj);
    keys.sort();

    let sortedObject:any = {};
    for (var i = 0; i < keys.length; i++) {
        var key = keys[i];
        sortedObject[key] = obj[key];
    }

    return sortedObject;
}

export default {
    sortObject
}