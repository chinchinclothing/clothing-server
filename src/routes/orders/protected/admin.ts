import {Router} from 'express';
import authorization from '../../../auth/authorization';
import authentication from '../../../auth/authentication';
import { e_role_code as RoleCode } from '@prisma/client';
import accessRoles from '../../../helpers/accessRoles'
import validator ,{ValidationSource} from '../../../helpers/validator';
import asyncHandler from '../../../helpers/asyncHandler';
import {ProtectedRequest, PublicRequest} from 'app-request';
import * as userServices from '../../../services/users';
import { e_voucher_target as EVoucherTarget,e_voucher_type as EVoucherType } from '@prisma/client';
import schema from '../schema';
import voucherServices from '../../../services/vouchers';
import deliveryServices from '../../../services/deliveries';
import variantServices from '../../../services/variants';
import shipmentServices from '../../../services/shipment';
import orderServices from '../../../services/orders';
import paymentServices from '../../../services/payment';
import settingServices from '../../../services/setting';

import { SuccessResponse } from '../../../core/ApiResponse';
import { BadRequestError } from '../../../core/ApiError';
import {Prisma} from '@prisma/client';
import _ from 'lodash';
//================================================================
const adminRouter = Router();
adminRouter.use(
    authentication,
    accessRoles(RoleCode.ADMIN, RoleCode.MANAGER),
    authorization,
);
//================================================================
/**
 * 
 * Retrieve list of orders
 * 
 */
adminRouter.get(
    '/',
    validator(schema.adminRetrieveListOrers, ValidationSource.QUERY),
    asyncHandler(async (req: ProtectedRequest, res) => {
        const orders = await orderServices.adminRetrieveListOrers(req.query);
        new SuccessResponse('Success', {
            orders:orders.orders,
            page:orders.page,
            totalPage:orders.totalPage,
        }).send(res);
        
    })
)


/**
 * 
 * Retrieve order by id
 * 
 */
adminRouter.get(
    '/:id',
    validator(schema.createOrder),
    asyncHandler(async (req: ProtectedRequest, res) => {
    
    })
)

/**
 * 
 * update status
 * 
 */
adminRouter.put(
    '/:id/status',
    validator(schema.adminUpdateOrderStatus),
    validator(schema.orderID, ValidationSource.PARAM),
    asyncHandler(async (req: ProtectedRequest, res) => {
        const orderId = parseInt(req.params.id);
        const order = await orderServices.getOrderById(orderId);
        if(!order) throw new BadRequestError('Order not found');
        if(req.body.status === 'CANCELLED') throw new BadRequestError('API not support cancel order, use cancel API');
        if(order.status === 'COMPLETED') throw new BadRequestError('Order is completed');
        if(order.status === 'CANCELLED') throw new BadRequestError('Order is cancelled');
        const updateData = {
            orderId: orderId,
            userId: req.user.id,
            status: req.body.status
        }
        const updateOrder = await orderServices.updateOrderStatus(updateData);
        if(!updateOrder) throw new BadRequestError('Update order status fail');
        new SuccessResponse('Update order status success', {order:updateOrder}).send(res);
    })
)


/**
 * 
 * Cancel order
 * 
 */

adminRouter.post(
    '/cancel',
    validator(schema.adminCancelledOrder),
    asyncHandler(async (req: ProtectedRequest, res) => {
        const orderId = parseInt(req.body.order_id);
        const order = await orderServices.getOrderById(orderId);
        if(!order) throw new BadRequestError('Order not found');
        console.log("order.status",order.status);
        if( !order.status||order.status && !["WAITING_PAYMENT","WAITING_CONFIRMATION","WAITING_PICKUP"].includes(order.status) ) throw new BadRequestError('Order can not be cancel');
        const cancelData = {
            orderId: orderId,
            userId: req.user.id,
            reason: req.body.reason,
        }
        
        //refund
        let isRefund = true;
        if(order.payment_method === 'VNPAY' && order.payment_status === 'PAID', order.payment_at){
            //get detail payment
            const refundResult = await paymentServices.refundVNPayTransaction(orderId, req.user.id);
            console.log("refundResult",refundResult);
            if(!refundResult) {
               isRefund = false
            };
        }

        const cancelOrder = await orderServices.adminCancelOrder(cancelData);
        
        if(!cancelOrder) throw new BadRequestError('Cancel order fail');

        new SuccessResponse('Cancel order success', {
            order:cancelOrder,
            refund: {
                isRefund,
                message: isRefund ? 'Refund success' : 'Refund fail, request refund again'
            }
        }).send(res);
    })
)


/**
 * 
 * Update order
 * 
 */

//================================================================
export default adminRouter;


