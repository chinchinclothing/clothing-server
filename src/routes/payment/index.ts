import protectedRouter from './protected';
import publicRouter from './public';

export default {
    protected: protectedRouter,
    public:    publicRouter
}