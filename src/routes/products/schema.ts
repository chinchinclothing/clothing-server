import Joi from "joi";
import { products, e_product_status, e_gender } from "@prisma/client";

export default {
    productCreate: Joi.object().keys({
        title: Joi.string().required(),
        meta_title: Joi.string(),
        slug: Joi.string(),
        summary: Joi.string(),
        gender: Joi.string().valid(...Object.keys(e_gender)),
        sku: Joi.string(),
        status: Joi.string().valid(...Object.keys(e_product_status)),
        content: Joi.string()
    }),
    productFilters: Joi.object().keys({
        status: Joi.string().valid(...Object.keys(e_product_status)),
        title: Joi.string(),
        meta_title: Joi.string(),
        slug: Joi.string(),
        sku: Joi.string(),
        min_price: Joi.number(),    
        max_price: Joi.number(),
        gender: Joi.string().valid(...Object.keys(e_gender)),
        from:Joi.date(),
        to:Joi.date(),
        limit: Joi.number(),
        page: Joi.number(),
        published: Joi.boolean()
    }),
    productUniqueParams: Joi.object().keys({
        type: Joi.string().valid('slug','id').required(),
        value: Joi.string().required()
    }),
    productCountQueries: Joi.object().keys({
        status: Joi.string().valid(...Object.keys(e_product_status)),
        category_id: Joi.string(),
        created_at_min: Joi.date(),
        created_at_max: Joi.date(),
        updated_at_min: Joi.date(),
        updated_at_max: Joi.date(),
    }),
    productId: Joi.object().keys({
        id: Joi.number().required()
    }),
    variantCreate: Joi.object().keys({
        color_id: Joi.number().required(),
        size_id: Joi.number().required(),
    }),
    variantIdUpdate: Joi.object().keys({
        id: Joi.number().required(),
        variant_id: Joi.number().required()
    }),
    variantId: Joi.object().keys({
        variant_id: Joi.number().required()
    }),
    variantUpdate: Joi.object().keys({
        color_id: Joi.number(),
        size_id: Joi.number(),
    }),
    variantPriceCreate: Joi.object().keys({
        price: Joi.number().required(),
        currency_id: Joi.number().required(),
        tax_percent: Joi.number(),
        compare_at_price: Joi.number(),
    }),
    variantPriceUpdate: Joi.object().keys({
        price: Joi.number(),
        currency_id: Joi.number(),
        tax_percent: Joi.number(),
        compare_at_price: Joi.number(),
    }),
    priceId: Joi.object().keys({
        price_id: Joi.number().required()
    }),
    imagesCreate: Joi.object().keys({
        images: Joi.array().items(
            Joi.object().keys({
                color_id: Joi.number().required(),
                url: Joi.string().required(),
                position: Joi.number()
            })
        ).required(),
    }),
    imagesDelete: Joi.object().keys({
        image_ids: Joi.array().items(Joi.number()).required()
    }),
    videosCreate: Joi.object().keys({
        videos: Joi.array().items(
            Joi.object().keys({
                url: Joi.string().required(),
            })
        ).required(),
    }),
    videosDelete: Joi.object().keys({
        video_ids: Joi.array().items(Joi.number()).required()
    }),
}