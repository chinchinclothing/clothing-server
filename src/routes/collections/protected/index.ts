import { Router } from "express";
import asyncHandler from "../../../helpers/asyncHandler";
import authorization from "../../../auth/authorization";
import authentication from "../../../auth/authentication";
import accessRoles from "../../../helpers/accessRoles";
import { e_role_code as RoleCode } from "@prisma/client";
import validator from "../../../helpers/validator";
import collectionService from "../../../services/collections";
import {SuccessResponse} from "../../../core/ApiResponse";
import { BadRequestError } from "../../../core/ApiError";
import { ValidationSource } from "../../../helpers/validator";
import { ProtectedRequest } from "app-request";
import schema from "../schema";

const router = Router();

router.use(
    authentication,
    accessRoles(RoleCode.ADMIN, RoleCode.MANAGER),
    authorization,
);

router.post(
  "/",
  validator(schema.createCollection),
  asyncHandler(async (req: ProtectedRequest, res) => {
    console.log("start==============");
    //create collection
    let collectionData = req.body;
    console.log("published", typeof collectionData.published);
    if (collectionData.published) {
      collectionData.published_at = new Date();
      delete collectionData.published;
    } else {
      delete collectionData.published;
    }
    const collection = await collectionService.create(req.body);
    new SuccessResponse("Collection created successfully", collection).send(
      res
    );
  })
);

router.get(
  "/",
  validator(schema.collectionFilter, ValidationSource.QUERY),
  asyncHandler(async (req: ProtectedRequest, res) => {
    const collections = await collectionService.findAll(req.query);
    new SuccessResponse("Collections found", { collections }).send(res);
  })
);

router.get(
  "/:id",
  validator(schema.collectionId, ValidationSource.PARAM),
  asyncHandler(async (req: ProtectedRequest, res) => {
    const collection = await collectionService.findByPk(
      parseInt(req.params.id)
    );
    if (!collection) throw new BadRequestError("Collection not found");
    new SuccessResponse("Collection found", { collection }).send(res);
  })
);

router.put(
  "/:id",
  validator(schema.collectionId, ValidationSource.PARAM),
  validator(schema.updateCollection),
  asyncHandler(async (req: ProtectedRequest, res) => {
    const collection = await collectionService.findByPk(
      parseInt(req.params.id)
    );
    if (!collection) throw new BadRequestError("Collection not found");

    let collectionData = req.body;
    if (collectionData.published === true) {
      collectionData.published_at = new Date();
    }
    if (collectionData.published === false) {
      collectionData.published_at = null;
    }

    delete collectionData.published;

    const updatedCollection = await collectionService.update(
      parseInt(req.params.id),
      collectionData
    );

    new SuccessResponse("Collection updated", { updatedCollection }).send(res);
  })
);

router.delete(
    '/:id/images',
    validator(schema.collectionId, ValidationSource.PARAM),
    validator(schema.collectionImageIdxs, ValidationSource.BODY),
    asyncHandler(async (req: ProtectedRequest, res) => {
        const collection = await collectionService.findByPk(parseInt(req.params.id));
        if (!collection) throw new BadRequestError('Collection not found');
        console.log("req.body",req.body)
        const updatedCollection = await collectionService.deleteImages(parseInt(req.params.id), req.body.image_idxs);
        
        new SuccessResponse(
            'Collection images deleted', 
            {updatedCollection}
        ).send(res);
    })
   
)   

/**
 * 
 * Add product to collection
 * 
 */
router.post(
    '/:id/products',
    validator(schema.collectionId, ValidationSource.PARAM),
    validator(schema.productIds, ValidationSource.BODY),
    asyncHandler(async (req: ProtectedRequest, res) => {
        //check if collection exists
        const collection = await collectionService.findByPk(parseInt(req.params.id));
        if (!collection) throw new BadRequestError('Collection not found');
        
        //add products to collection
        const updatedCollection = await collectionService.addProducts(
            parseInt(req.params.id),
            req.body.product_ids
        );

        //send response
        new SuccessResponse(
            'Products added to collection', 
            {updatedCollection}
        ).send(res);
    })
)

/**
 * 
 * remove product from collection
 * 
 */

router.delete(
    '/:id/products',
    validator(schema.collectionId, ValidationSource.PARAM),
    validator(schema.productIds, ValidationSource.BODY),
    asyncHandler(async (req: ProtectedRequest, res) => {
        //check if collection exists
        const collection = await collectionService.findByPk(parseInt(req.params.id));
        if (!collection) throw new BadRequestError('Collection not found');
        
        //remove products from collection
        const updatedCollection = await collectionService.removeProducts(
            parseInt(req.params.id),
            req.body.product_ids
        );

        //send response
        new SuccessResponse(
            'Products removed from collection', 
            {updatedCollection}
        ).send(res);
    })
)

/**
 * Retrieve hightlight of collections
 */

router.get(
    '/:id/highlights',
    asyncHandler(
        async (req:ProtectedRequest, res)=>{
            const collectionExist = await collectionService.findByPk(parseInt(req.params.id));
            if(!collectionExist){
                throw new BadRequestError("Collection not found");
            }

            //get highlight
            const collection = await collectionService.findHighLightCollection(parseInt(req.params.id));
            return new SuccessResponse(
                'Get succes',
                {
                    collection
                }
            ).send(res);
        }
    )
)



export default router;