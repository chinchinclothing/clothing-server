import collections from "..";
import prisma from "../../../db/prisma/connection"
import {Prisma} from '@prisma/client';

async function create (values: Prisma.highlight_collectionsCreateInput ){
    const newHighlight = await prisma.highlight_collections.create(
        {
            data: values
        }
    )
    return newHighlight;
}

async function findByPk(id:number){
    const highlight = await prisma.highlight_collections.findUnique({
        where:{
            id
        }
    })
    return highlight;
}

async function update (id:number, data:Prisma.highlight_collectionsUpdateInput){
    const highlightUpdate = await prisma.highlight_collections.update({
        where:{
            id
        },
        data
    });

    return highlightUpdate;
}

async function findAll(filters: any){
    const where= {
        AND:[
            filters.published===true?{
                published_at:{
                    not: null
                }
            }:filters.published===false?{
                published_at:null
            }:{},
            filters?.title?{
                collections:{
                    title:{
                        contains:filters.title
                    }
                }
            }:{},
            {
                OR:[
                    filters.published_scope?
                        filters.published_scope === 'all'?
                        {
                            collections:{
                                published_scope:{
                                    in:['web', 'mobile', 'all']
                                }
                            }
                        }:{
                            collections:{
                                published_scope:{
                                    in:[filters.published_scope, 'all']
                                }
                            }
                        }
                    :{}
                ]
            },
            filters.published_at_min && filters.published_at_max?{
                published_at : {
                    gte: new Date(filters.published_at_min),
                    lte: new Date(filters.published_at_max)
                }
            }:filters.published_at_min?{
                published_at : {
                    gte: new Date(filters.published_at_min),
                }
            }:filters.published_at_max?{
                published_at : {
                    lte: new Date(filters.published_at_max),
                }
            }:{},
            
            filters.created_at_min && filters.created_at_max?{
                createdat : {
                    gte: new Date(filters.created_at_min),
                    lte: new Date(filters.created_at_max) 
                }
            }:filters.created_at_min?{
                createdat : {
                    gte: new Date(filters.created_at_min),
                }
            }:filters.created_at_max?{
                createdat : {
                    lte: new Date(filters.created_at_max),
                }
            }:{},
            filters.status?{
                status:filters.status
            }:{}
        ]
    }

    console.log("where",where.AND[2].published_at)
    const highlights = await prisma.highlight_collections.findMany(
        {
            where,
            include:{
                collections:{
                    include:{
                        collection_image: true
                    }
                },
                
            }
            
        }
    )
    return highlights;
}
export default{
    create,
    findByPk,
    update,
    findAll
}