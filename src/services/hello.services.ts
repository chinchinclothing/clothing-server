const sayHello = () => {
    return 'Hello World!';
}

const sayGoodbye = () => {
    return 'Goodbye!';
}

export { sayHello, sayGoodbye };