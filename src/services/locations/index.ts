import {Prisma} from '@prisma/client'
import prisma from '../../db/prisma/connection';
import _ from 'lodash';

async function create(location:Prisma.locationsCreateInput){
    try {
        const newLocation = await prisma.locations.create({
            data:location
        })
        return newLocation;
    } catch (error) {
        throw error;
    }
}

async function createBulk(locations:Prisma.locationsCreateManyInput[]){
    try {
        const newLocations = await prisma.locations.createMany({
            data:locations
        })
        return newLocations;
    } catch (error) {
        throw error;
    }
} 

async function getSubLocationById(id:number|null){
    try {
        console.log(id);
        const locations = await prisma.locations.findMany({
            where:{
                parent_id:id
            }
        })
        return locations;
    } catch (error) {
        throw error;
    }
}

async function update(id:number, location:Prisma.locationsUpdateInput){
    try {
        const updatedLocation = await prisma.locations.update({
            where:{
                id:id
            },
            data:location
        })
        return updatedLocation;
    } catch (error) {
        throw error;
    }
}   

async function getLocationById(id : number){
    try {
        const location = await prisma.locations.findFirst({
            where:{
                id
            }
        });
        return location;
    } catch (error) {
        throw error;
    }
}

export default {
    createBulk,
    create,
    update,
    getSubLocationById,
    getLocationById
}