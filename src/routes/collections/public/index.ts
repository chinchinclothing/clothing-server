import { Router } from "express";
import validator, { ValidationSource } from "../../../helpers/validator";
import schema from "../schema";
import asyncHandler from "../../../helpers/asyncHandler";
import { PublicRequest } from "app-request";
import collectionService from "../../../services/collections";
import { SuccessResponse } from "../../../core/ApiResponse";
import { BadRequestError } from "../../../core/ApiError";

const router = Router();

router.get(
    '/',
    validator(schema.collectionFilter, ValidationSource.QUERY),
    asyncHandler(async (req: PublicRequest, res) => {
        req.query.published_status = 'true';
        const {collections, page, totalPage } = await collectionService.findAll(req.query);
        new SuccessResponse(
            'Collections found',
            {
                collections,
                meta:{
                    page,
                    totalPage
                }
            }
        ).send(res);
    })
)

router.get(
    '/highlight',
    validator(schema.collectionFilter, ValidationSource.QUERY),
    asyncHandler(async (req: PublicRequest, res) => {
        req.query.published_status = 'true';
        const collections = await collectionService.findHighLightCollection(req.query);
        new SuccessResponse(
            'Collections found',
            {
                collections,
            }
        ).send(res);
    })
)

router.get(
    '/:id/products',
    validator(schema.collectionId, ValidationSource.PARAM),
    asyncHandler(async (req: PublicRequest, res) => {
        const {products, totalPage, page} = await collectionService.findProductsOfCollection(parseInt(req.params.id), req.query);
        if (!products) throw new BadRequestError('Collection not found');
        new SuccessResponse(
            'Collection found',
            {   
                products,
                meta:{
                    page,
                    totalPage
                }
            }
        ).send(res);
    })
)


export default router;