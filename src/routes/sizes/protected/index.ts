import {Router} from 'express';
import authorization from '../../../auth/authorization';
import authentication from '../../../auth/authentication';
import { e_role_code as RoleCode } from '@prisma/client';
import accessRoles from '../../../helpers/accessRoles'
import validator ,{ValidationSource} from '../../../helpers/validator';
import asyncHandler from '../../../helpers/asyncHandler';
import {ProtectedRequest, PublicRequest} from 'app-request';

import schema from '../schema';
import sizesService from '../../../services/sizes';

import { SuccessResponse } from '../../../core/ApiResponse';
import { BadRequestError } from '../../../core/ApiError';

//================================================================

const router = Router();

//================================================================

router.post(
    '/',
    validator(schema.sizeCreate),
    asyncHandler(async (req: ProtectedRequest, res) => {

        // Check if the size already exists
        const sizeExists = await sizesService.findSize(req.body);
        if (sizeExists) throw new BadRequestError('Size already exists');

        // Create the size 
        const size = await sizesService.create(req.body);
        new SuccessResponse('Size created successfully', {size}).send(res);
    })
)

router.get(
    '/',
    validator(schema.sizeQuery, ValidationSource.QUERY),
    asyncHandler(async (req: ProtectedRequest, res) => {
        const sizes = await sizesService.findAll(req.query);
        new SuccessResponse(
            'Sizes found', 
            {sizes}
        ).send(res);
    })
)

router.get(
    '/:id',
    validator(schema.sizeId, ValidationSource.PARAM),
    asyncHandler(async (req: ProtectedRequest, res) => {
        const size = await sizesService.findByPk(parseInt(req.params.id));
        if (!size) throw new BadRequestError('Size not found');
        new SuccessResponse(
            'Size found', 
            {size}
        ).send(res);
    })
)

router.put(
    '/:id',
    validator(schema.sizeId, ValidationSource.PARAM),
    validator(schema.sizeUpdate),
    asyncHandler(async (req: ProtectedRequest, res) => {
        
        // Check if the size already exists
        const sizeExists = await sizesService.findByPk(parseInt(req.params.id));
        if (!sizeExists) throw new BadRequestError('Size not found');

        // Check if the size already exists
        const sizeExists2 = await sizesService.findSize(req.body);
        if (
            sizeExists2
            && sizeExists2.id !== parseInt(req.params.id)
        ) throw new BadRequestError('Size already exists');

        // Update the size
        const size = await sizesService.update(parseInt(req.params.id), req.body);
        new SuccessResponse('Size Updated successfully', {size}).send(res);
    })
)



export default router;