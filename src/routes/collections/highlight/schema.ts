import Joi, { object } from "joi";
import {e_highlightcollection_status as EStatus, e_highlightcollecitons_type as EType} from '@prisma/client';

export default {
    newHighlight: Joi.object().keys({
        collection_id: Joi.number(),
        position: Joi.number(),
        status: Joi.string().valid(...Object.keys(EStatus)),
        height: Joi.number(),
        width: Joi.number(),
        published_at: Joi.date(),
        type: Joi.string().valid(...Object.keys(EType))
    }),
    updateHighlight: Joi.object().keys({
        position: Joi.number(),
        status: Joi.string().valid(...Object.keys(EStatus)),
        height: Joi.number(),
        width: Joi.number(),
        published_at: Joi.date(),
        type: Joi.string().valid(...Object.keys(EType))
    }),
    filterHighlight: Joi.object().keys({
        title: Joi.string(),
        published_scope: Joi.date(),
        published_at_min: Joi.date(),
        published_at_max: Joi.date(),
        published: Joi.boolean(),
        created_at_min: Joi.date(),
        created_at_max: Joi.date(),
        status: Joi.string(),
    })
}