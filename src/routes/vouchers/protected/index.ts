import {Router} from 'express';
import authorization from '../../../auth/authorization';
import authentication from '../../../auth/authentication';
import { e_role_code as RoleCode } from '@prisma/client';
import accessRoles from '../../../helpers/accessRoles'
import validator ,{ValidationSource} from '../../../helpers/validator';
import asyncHandler from '../../../helpers/asyncHandler';
import {ProtectedRequest, PublicRequest} from 'app-request';
import * as userServices from '../../../services/users';
import { e_voucher_target as EVoucherTarget,e_voucher_type as EVoucherType } from '@prisma/client';
import schema from '../schema';
import voucherServices from '../../../services/vouchers';

import { SuccessResponse } from '../../../core/ApiResponse';
import { BadRequestError } from '../../../core/ApiError';
import _ from 'lodash';
//================================================================
const router = Router();
router.use(
    authentication,
);
//================================================================
/**
 * definde role access
 */
const managerAuth = Router();
managerAuth.use(
    accessRoles(RoleCode.ADMIN, RoleCode.MANAGER),
    authorization,
);
//================================================================
/**
 * 
 * create new voucher
 * 
 */

router.post(
    '/', 
    managerAuth,
    validator(schema.voucher), 
    asyncHandler(async (req: ProtectedRequest, res) => {
        const voucher = await voucherServices.createVoucher(req.body);
        new SuccessResponse('Voucher created successfully', voucher).send(res);
    })
);

/**
 *
 * update voucher
 * 
 */
router.put(
    '/:id',
    managerAuth,
    validator(schema.voucherId, ValidationSource.PARAM),
    validator(schema.voucherUpdate),
    asyncHandler(async (req: ProtectedRequest, res) => {
        console.log(req.user);
        const voucher = await voucherServices.updateVoucher(parseInt(req.params.id), req.body);
        new SuccessResponse('Voucher updated successfully', voucher).send(res);
    })
);


/**
 * 
 * Add voucher for user
 * 
 */
router.post(
    '/:id/users',
    managerAuth,
    validator(schema.voucherId, ValidationSource.PARAM),
    validator(schema.voucherAdd),
    asyncHandler(async (req: ProtectedRequest, res) => {
        // check if user exists
        const users = await userServices.findAllByManyId(req.body.user_ids);
        const userIds = users.map((user)=>user.id);
        
        //check user exist
        const userNotExist = _.differenceWith(req.body.user_ids, userIds, _.isEqual);
        const usersExist = _.intersectionWith(req.body.user_ids,userIds, _.isEqual);
        
        //check if user already have the voucher
        const userPromoExist = await voucherServices.findVoucherExist(parseInt(req.params.id), userIds);
        const userIdsOfUserPromoExist = userPromoExist.map((userPromo)=>userPromo.user_id);
        
        //list of users that are not in user_promo
        const usersExistNotInUserPromo = _.differenceWith(usersExist, userIdsOfUserPromoExist, _.isEqual);
        const usersExistInUserPromo = _.intersectionWith(usersExist, userIdsOfUserPromoExist, _.isEqual);


        const voucher = await voucherServices.addVoucherForUser(parseInt(req.params.id), usersExistNotInUserPromo);

        // Create result object
        const result = {
            created: {
                user_ids: usersExistNotInUserPromo,
            },
            ...(usersExistInUserPromo.length > 0 && {
            exist: {
                user_ids: usersExistInUserPromo,
            }
            }),
            ...(userNotExist.length > 0 && {
                failed: {
                    user_ids: userNotExist,
                    error: {
                        message: 'User not found'
                    }
                }
            })
        };
  
        // Send success response
        new SuccessResponse('Voucher added successfully', result).send(res);
    })
)

/**
 * 
 * Retrieves list voucher of users
 * 
 */
router.get(
    '/users/vouchers',
    asyncHandler(async (req: ProtectedRequest, res) => {
        const user = req.user
        const vouchers = await voucherServices.findVouchersByFilters({ 
            quantity: {
                gt: 0
            },
            end_at:{gt:new Date()},
            start_at:{lt:new Date()},
            OR:[
                {user_promo:{some:{user_id:user.id}}},
                {target_type:'public'}
            ]
        });
        new SuccessResponse('Users found successfully', {vouchers}).send(res);
    })
);

/**
 * 
 * Remove list vouchers
 * 
 */
router.get(
    '/',
    validator(schema.voucherFilters, ValidationSource.QUERY),
    asyncHandler(async (req: ProtectedRequest, res) => {
        const filters = {
            AND:[
                req.query.user_id?({user_promo:{some:{user_id:parseInt(req.query.user_id as string)}}}):{},
                req.query.title?{title:{contains:req.query.title as string}}:{},
                req.query.code?{code:{contains:req.query.code as string}}:{},
                req.query.start_at?{start_at:{gt:new Date(req.query.start_at as string)}}:{},
                req.query.end_at?{end_at:{lt:new Date(req.query.start_at as string)}}:{},
                req.query.target_type?{target_type:req.query.target_type as EVoucherTarget}:{},
                req.query.discount_type?{discount_type:req.query.discount_type as EVoucherType}:{}
            ]
        };
        
        const vouchers = await voucherServices.findVouchersByFilters({
            AND:[
                req.query.user_id?({user_promo:{some:{user_id:parseInt(req.query.user_id as string)}}}):{},
                req.query.title?{title:{contains:req.query.title as string, mode:'insensitive'}}:{},
                req.query.code?{code:{contains:req.query.code as string, mode:'insensitive'}}:{},
                req.query.start_at?{start_at:{gt:new Date(req.query.start_at as string)}}:{},
                req.query.end_at?{end_at:{lt:new Date(req.query.start_at as string)}}:{},
                req.query.target_type?{target_type:req.query.target_type as EVoucherTarget}:{},
                req.query.discount_type?{discount_type:req.query.discount_type as EVoucherType}:{}
            ]
        });
        return new SuccessResponse('Users found successfully', {vouchers}).send(res);
    })
);

/**
 * 
 * Retrieves voucher by id
 * 
 */
router.get(
    '/:id',
    validator(schema.voucherId, ValidationSource.PARAM),
    asyncHandler(async (req: ProtectedRequest, res) => {
        const voucher = await voucherServices.findVoucherById(parseInt(req.params.id));
        if(!voucher) throw new BadRequestError('Voucher not found');
        new SuccessResponse('Voucher found successfully', {voucher}).send(res);
    })
);


//================================================================
export default router;