import {Router} from 'express';
import authorization from '../../../auth/authorization';
import authentication from '../../../auth/authentication';
import { e_role_code as RoleCode } from '@prisma/client';
import accessRoles from '../../../helpers/accessRoles'
import validator ,{ValidationSource} from '../../../helpers/validator';
import asyncHandler from '../../../helpers/asyncHandler';
import {ProtectedRequest, PublicRequest} from 'app-request';

import schema from '../schema';
import locationServices from '../../../services/locations';
import deliverServices from '../../../services/deliveries';

import { SuccessResponse } from '../../../core/ApiResponse';
import { BadRequestError } from '../../../core/ApiError';
import _ from 'lodash';
//================================================================
const router = Router();
//================================================================
router.use(
    authentication,
);
//================================================================

/**
 * 
 * create new location
 * 
 */
router.post(
    '/bulk',
    validator(schema.locations),
    asyncHandler(async (req: ProtectedRequest, res) => {
        const locations = await locationServices.createBulk(req.body.locations);
        new SuccessResponse('Location created successfully', {locations}).send(res);
    })
)



/**
 * 
 * update location
 * 
 */
router.put(
    '/:id',
    validator(schema.location),
    asyncHandler(async (req: ProtectedRequest, res) => {
        const location = await locationServices.update(parseInt(req.params.id), req.body);
        new SuccessResponse('Location updated successfully', {location}).send(res);
    })
)

/**
 * 
 * add master location
 * 
 */
router.post(
    '/deliveries/master',
    validator(schema.masterLocation),
    asyncHandler(async (req: ProtectedRequest, res) => {
        const data = {
            user_created_id: req.user.id,
            user_updated_id: req.user.id,
            ...req.body
        };
        const location = await deliverServices.addMasterAddress(data);
        new SuccessResponse('Location created successfully', {location}).send(res);
    })
)

router.put(
    '/deliveries/master',
    validator(schema.masterLocation),
    asyncHandler(async (req: ProtectedRequest, res) => {
        const data = {
            user_created_id: req.user.id,
            ...req.body
        };
        const masterLocation = await deliverServices.getMasterAddress();
        if(!masterLocation){
            throw new BadRequestError("Master location not found");
        }

        delete data.status;
        data.user_updated_id = req.user.id;
        const location = await deliverServices.updateMasterAddress(masterLocation.id, data);
        new SuccessResponse('Location created successfully', {location}).send(res);
    })
)


//================================================================
export default router;