import {Router} from 'express';
import asyncHandler from '../../helpers/asyncHandler';
import { ProtectedRequest } from 'app-request';
import { AuthFailureError } from '../../core/ApiError';
import { TokenRefreshResponse } from '../../core/ApiResponse';
import validator, {ValidationSource} from '../../helpers/validator';
import schema from './schema';
import { getAccessToken, validateTokenData, createTokens } from '../../auth/authUtils';
import JWT from '../../core/JWT';
import * as UserService from '../../services/users'
import * as KeystoreService from '../../services/keystore';
import crypto from 'crypto';

//=========================================================

const router = Router();

//=========================================================

router.post('/refresh',
    validator(schema.auth,ValidationSource.HEADER),
    validator(schema.refreshToken),
    asyncHandler(async (req:ProtectedRequest, res) => {
        //get access token from header
        req.accessToken = getAccessToken(req.headers.authorization);

        //decode access token   
        const accessTokenPayload = await JWT.decode(req.accessToken);

        //validate access token
        validateTokenData(accessTokenPayload);
        
        //find user by id
        const user = await UserService.findById(parseInt(accessTokenPayload.sub));

        //check user exist
        if(!user) throw new AuthFailureError('User not registered');
        req.user = user;

        //decode refresh token
        const refreshTokenPayload = await JWT.decode(req.body.refreshToken);

        //validate refresh token
        validateTokenData(refreshTokenPayload);

        //compare access token and refresh token
        if(accessTokenPayload.sub !== refreshTokenPayload.sub) throw new AuthFailureError('Invalid access token');

        //check keystore
        const keystore = await KeystoreService.find(user.id,accessTokenPayload.prm,refreshTokenPayload.prm);
        
        //keystore not found
        if(!keystore) throw new AuthFailureError('Invalid access token');

        //remove keystore
        await KeystoreService.remove(keystore.id);

        //create new key
        const accessTokenKey = crypto.randomBytes(64).toString('hex');
        const refreshTokenKey = crypto.randomBytes(64).toString('hex');

        //save new key
        await KeystoreService.create(user.id,accessTokenKey,refreshTokenKey);

        //create new token
        const tokens = await createTokens(user,accessTokenKey,refreshTokenKey);

        //return success response
        new TokenRefreshResponse(
            'Token Issued',
            tokens.accessToken,
            tokens.refreshToken
        ).send(res);
    })  
)

//=========================================================

export default router;