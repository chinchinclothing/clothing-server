import {Router} from 'express';
import validator from '../../helpers/validator';
import schema from './schema';
import asyncHandler from '../../helpers/asyncHandler';
import { RoleRequest } from 'app-request';
import { BadRequestError,InternalError } from '../../core/ApiError';
import * as UserService from '../../services/users';
import * as KeystoreService from '../../services/keystore';
import { createTokens } from '../../auth/authUtils';
import { SuccessResponse } from '../../core/ApiResponse';
import { hash } from '../../auth/authUtils';
import _ from 'lodash';
import crypto from 'crypto';

const router = Router();

router.post('/', 
    validator(schema.signup),
    asyncHandler(async (req:RoleRequest, res) => {
        //check user exist
        const user = await UserService.findByEmail(req.body.email);
        if(user) throw new BadRequestError('User already registered');
        //hash password
        req.body.password = await hash(req.body.password);
        //create user
        const newUser = await UserService.create(req.body, 'USER');
        if(!newUser?.id) throw new InternalError('Failed to create user');

        //create key
        const accessTokenKey = crypto.randomBytes(64).toString('hex');
        const refreshTokenKey = crypto.randomBytes(64).toString('hex');
        
        //save key
        await KeystoreService.create(newUser.id,accessTokenKey,refreshTokenKey);

        //create token
        const tokens = await createTokens(newUser,accessTokenKey,refreshTokenKey);

        //return success response
        new SuccessResponse(
            'Signup Successful',
            {
                user:_.pick(newUser,['id','username','full_name','profile_pic_url']),
                tokens
            }
        ).send(res);
    })      
)

export default router;