-- AlterTable
ALTER TABLE "users" ALTER COLUMN "status" SET DEFAULT 'active';

-- AddForeignKey
ALTER TABLE "keystore" ADD CONSTRAINT "keystore_user_id_fkey" FOREIGN KEY ("user_id") REFERENCES "users"("id") ON DELETE NO ACTION ON UPDATE NO ACTION;
