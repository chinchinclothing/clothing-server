import {Router} from 'express';
import authorization from '../../../auth/authorization';
import authentication from '../../../auth/authentication';
import { e_role_code as RoleCode } from '@prisma/client';
import accessRoles from '../../../helpers/accessRoles'
import validator ,{ValidationSource} from '../../../helpers/validator';
import asyncHandler from '../../../helpers/asyncHandler';
import {ProtectedRequest, PublicRequest} from 'app-request';

import schema from '../schema';
import shipmentServices from '../../../services/shipment';

import { SuccessResponse } from '../../../core/ApiResponse';
import { BadRequestError } from '../../../core/ApiError';
import _ from 'lodash';
//================================================================
const router = Router();
//================================================================
/**
 * 
 * Calculate shipping cost
 * 
 */
router.post(
    '/shipping-cost',
    validator(schema.shippingCost),
    asyncHandler(async (req: PublicRequest, res) => {
        const cost = await shipmentServices.calculateShippingCost(parseFloat(req.body.parcel_info.parcel_weight), parseInt(req.body.sender_info.sender_state_location_id), parseInt(req.body.deliver_info.deliver_state_location_id));
        new SuccessResponse('Shipping cost calculated successfully', {
            fee_info: [
                {
                    estimated_shipping_fee: cost,
                    parcel_weight: req.body.parcel_info.parcel_weight,
                }
            ]
        }).send(res);
    })
)

export default router;