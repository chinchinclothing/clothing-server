/*
  Warnings:

  - You are about to drop the column `price` on the `products` table. All the data in the column will be lost.
  - You are about to drop the column `color` on the `variants` table. All the data in the column will be lost.
  - You are about to drop the column `size` on the `variants` table. All the data in the column will be lost.
  - You are about to drop the column `stock` on the `variants` table. All the data in the column will be lost.

*/
-- CreateEnum
CREATE TYPE "e_curruncy_code" AS ENUM ('VND', 'USD', 'EUR');

-- CreateEnum
CREATE TYPE "e_curryncy_symbol" AS ENUM ('d', 's', 'e');

-- CreateEnum
CREATE TYPE "e_gender" AS ENUM ('male', 'female', 'unkown');

-- CreateEnum
CREATE TYPE "e_height_unit" AS ENUM ('cm', 'm', 'inch');

-- CreateEnum
CREATE TYPE "e_variants_status" AS ENUM ('active', 'inactive');

-- CreateEnum
CREATE TYPE "e_weight_unit" AS ENUM ('kg', 'g');

-- AlterTable
ALTER TABLE "products" DROP COLUMN "price",
ADD COLUMN     "gender" "e_gender",
ADD COLUMN     "quantity_sold" INTEGER;

-- AlterTable
ALTER TABLE "variants" DROP COLUMN "color",
DROP COLUMN "size",
DROP COLUMN "stock",
ADD COLUMN     "color_id" INTEGER,
ADD COLUMN     "size_id" INTEGER;

-- CreateTable
CREATE TABLE "body_measurements" (
    "id" SERIAL NOT NULL,
    "size_id" INTEGER,
    "product_id" INTEGER,
    "weightunit" "e_weight_unit",
    "weightmin" DOUBLE PRECISION,
    "weightmax" DOUBLE PRECISION,
    "heightunit" "e_height_unit",
    "heightmin" DOUBLE PRECISION,
    "heightmax" DOUBLE PRECISION,

    CONSTRAINT "body_measurements_pkey" PRIMARY KEY ("id")
);

-- CreateTable
CREATE TABLE "colors" (
    "id" SERIAL NOT NULL,
    "color" VARCHAR(255),
    "code" VARCHAR(255),

    CONSTRAINT "colors_pkey" PRIMARY KEY ("id")
);

-- CreateTable
CREATE TABLE "currency" (
    "id" SERIAL NOT NULL,
    "currency_code" "e_curruncy_code",
    "currency_symbol" "e_curryncy_symbol",

    CONSTRAINT "currency_pkey" PRIMARY KEY ("id")
);

-- CreateTable
CREATE TABLE "images" (
    "id" SERIAL NOT NULL,
    "product_id" INTEGER,
    "color_id" INTEGER,
    "url" TEXT,
    "position" INTEGER,

    CONSTRAINT "images_pkey" PRIMARY KEY ("id")
);

-- CreateTable
CREATE TABLE "prices" (
    "id" SERIAL NOT NULL,
    "variant_id" INTEGER,
    "price" DOUBLE PRECISION,
    "currency_id" INTEGER,
    "tax_percent" DOUBLE PRECISION,
    "compare_at_price" INTEGER,

    CONSTRAINT "prices_pkey" PRIMARY KEY ("id")
);

-- CreateTable
CREATE TABLE "sizes" (
    "id" SERIAL NOT NULL,
    "size" VARCHAR(255),

    CONSTRAINT "sizes_pkey" PRIMARY KEY ("id")
);

-- CreateTable
CREATE TABLE "videos" (
    "id" SERIAL NOT NULL,
    "product_id" INTEGER,
    "url" TEXT,

    CONSTRAINT "videos_pkey" PRIMARY KEY ("id")
);

-- AddForeignKey
ALTER TABLE "variants" ADD CONSTRAINT "variant_color_id_fkey" FOREIGN KEY ("color_id") REFERENCES "colors"("id") ON DELETE NO ACTION ON UPDATE NO ACTION;

-- AddForeignKey
ALTER TABLE "variants" ADD CONSTRAINT "variant_size_id_fkey" FOREIGN KEY ("size_id") REFERENCES "sizes"("id") ON DELETE NO ACTION ON UPDATE NO ACTION;

-- AddForeignKey
ALTER TABLE "body_measurements" ADD CONSTRAINT "body_measurements_product_id_fkey" FOREIGN KEY ("product_id") REFERENCES "products"("id") ON DELETE NO ACTION ON UPDATE NO ACTION;

-- AddForeignKey
ALTER TABLE "body_measurements" ADD CONSTRAINT "body_measurements_size_id_fkey" FOREIGN KEY ("size_id") REFERENCES "sizes"("id") ON DELETE NO ACTION ON UPDATE NO ACTION;

-- AddForeignKey
ALTER TABLE "images" ADD CONSTRAINT "images_color_id_fkey" FOREIGN KEY ("color_id") REFERENCES "colors"("id") ON DELETE NO ACTION ON UPDATE NO ACTION;

-- AddForeignKey
ALTER TABLE "images" ADD CONSTRAINT "images_product_id_fkey" FOREIGN KEY ("product_id") REFERENCES "products"("id") ON DELETE NO ACTION ON UPDATE NO ACTION;

-- AddForeignKey
ALTER TABLE "prices" ADD CONSTRAINT "prices_currency_id_fkey" FOREIGN KEY ("currency_id") REFERENCES "currency"("id") ON DELETE NO ACTION ON UPDATE NO ACTION;

-- AddForeignKey
ALTER TABLE "videos" ADD CONSTRAINT "videos_product_id_fkey" FOREIGN KEY ("product_id") REFERENCES "products"("id") ON DELETE NO ACTION ON UPDATE NO ACTION;
