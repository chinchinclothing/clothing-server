-- AlterTable
ALTER TABLE "carts" ALTER COLUMN "createdat" SET DATA TYPE TIMESTAMP(3),
ALTER COLUMN "updatedat" DROP DEFAULT,
ALTER COLUMN "updatedat" SET DATA TYPE TIMESTAMP(3);

-- AlterTable
ALTER TABLE "categories" ALTER COLUMN "createdat" SET DATA TYPE TIMESTAMP(3),
ALTER COLUMN "updatedat" DROP DEFAULT,
ALTER COLUMN "updatedat" SET DATA TYPE TIMESTAMP(3);

-- AlterTable
ALTER TABLE "category_tag" ALTER COLUMN "createdat" SET DATA TYPE TIMESTAMP(3),
ALTER COLUMN "updatedat" DROP DEFAULT,
ALTER COLUMN "updatedat" SET DATA TYPE TIMESTAMP(3);

-- AlterTable
ALTER TABLE "current_user_delivery" ALTER COLUMN "createdat" SET DATA TYPE TIMESTAMP(3),
ALTER COLUMN "updatedat" DROP DEFAULT,
ALTER COLUMN "updatedat" SET DATA TYPE TIMESTAMP(3);

-- AlterTable
ALTER TABLE "delivery_contact" ALTER COLUMN "createdat" SET DATA TYPE TIMESTAMP(3),
ALTER COLUMN "updatedat" DROP DEFAULT,
ALTER COLUMN "updatedat" SET DATA TYPE TIMESTAMP(3);

-- AlterTable
ALTER TABLE "inventory_transaction" ALTER COLUMN "createdat" SET DATA TYPE TIMESTAMP(3),
ALTER COLUMN "updatedat" DROP DEFAULT,
ALTER COLUMN "updatedat" SET DATA TYPE TIMESTAMP(3);

-- AlterTable
ALTER TABLE "order_canceled_detail" ALTER COLUMN "createdat" SET DATA TYPE TIMESTAMP(3),
ALTER COLUMN "updatedat" DROP DEFAULT,
ALTER COLUMN "updatedat" SET DATA TYPE TIMESTAMP(3);

-- AlterTable
ALTER TABLE "orders" ALTER COLUMN "createdat" SET DATA TYPE TIMESTAMP(3),
ALTER COLUMN "updatedat" DROP DEFAULT,
ALTER COLUMN "updatedat" SET DATA TYPE TIMESTAMP(3);

-- AlterTable
ALTER TABLE "product_category" ALTER COLUMN "createdat" SET DATA TYPE TIMESTAMP(3),
ALTER COLUMN "updatedat" DROP DEFAULT,
ALTER COLUMN "updatedat" SET DATA TYPE TIMESTAMP(3);

-- AlterTable
ALTER TABLE "product_on_sale" ALTER COLUMN "createdat" SET DATA TYPE TIMESTAMP(3),
ALTER COLUMN "updatedat" DROP DEFAULT,
ALTER COLUMN "updatedat" SET DATA TYPE TIMESTAMP(3);

-- AlterTable
ALTER TABLE "product_tag" ALTER COLUMN "createdat" SET DATA TYPE TIMESTAMP(3),
ALTER COLUMN "updatedat" DROP DEFAULT,
ALTER COLUMN "updatedat" SET DATA TYPE TIMESTAMP(3);

-- AlterTable
ALTER TABLE "products" ALTER COLUMN "createdat" SET DATA TYPE TIMESTAMP(3),
ALTER COLUMN "updatedat" DROP DEFAULT,
ALTER COLUMN "updatedat" SET DATA TYPE TIMESTAMP(3);

-- AlterTable
ALTER TABLE "promo" ALTER COLUMN "createdat" SET DATA TYPE TIMESTAMP(3),
ALTER COLUMN "updatedat" DROP DEFAULT,
ALTER COLUMN "updatedat" SET DATA TYPE TIMESTAMP(3);

-- AlterTable
ALTER TABLE "promo_log" ALTER COLUMN "createdat" SET DATA TYPE TIMESTAMP(3),
ALTER COLUMN "updatedat" DROP DEFAULT,
ALTER COLUMN "updatedat" SET DATA TYPE TIMESTAMP(3);

-- AlterTable
ALTER TABLE "roles" ALTER COLUMN "createdat" SET DATA TYPE TIMESTAMP(3),
ALTER COLUMN "updatedat" DROP DEFAULT,
ALTER COLUMN "updatedat" SET DATA TYPE TIMESTAMP(3);

-- AlterTable
ALTER TABLE "tags" ALTER COLUMN "createdat" SET DATA TYPE TIMESTAMP(3),
ALTER COLUMN "updatedat" DROP DEFAULT,
ALTER COLUMN "updatedat" SET DATA TYPE TIMESTAMP(3);

-- AlterTable
ALTER TABLE "transaction" ALTER COLUMN "createdat" SET DATA TYPE TIMESTAMP(3),
ALTER COLUMN "updatedat" DROP DEFAULT,
ALTER COLUMN "updatedat" SET DATA TYPE TIMESTAMP(3);

-- AlterTable
ALTER TABLE "user_promo" ALTER COLUMN "createdat" SET DATA TYPE TIMESTAMP(3),
ALTER COLUMN "updatedat" DROP DEFAULT,
ALTER COLUMN "updatedat" SET DATA TYPE TIMESTAMP(3);

-- AlterTable
ALTER TABLE "user_role" ALTER COLUMN "updatedat" DROP DEFAULT,
ALTER COLUMN "updatedat" SET DATA TYPE TIMESTAMP(3),
ALTER COLUMN "createdat" SET DATA TYPE TIMESTAMP(3);

-- AlterTable
ALTER TABLE "users" ALTER COLUMN "createdat" SET DATA TYPE TIMESTAMP(3),
ALTER COLUMN "updatedat" DROP DEFAULT,
ALTER COLUMN "updatedat" SET DATA TYPE TIMESTAMP(3);

-- AlterTable
ALTER TABLE "variants" ALTER COLUMN "createdat" SET DATA TYPE TIMESTAMP(3),
ALTER COLUMN "updatedat" DROP DEFAULT,
ALTER COLUMN "updatedat" SET DATA TYPE TIMESTAMP(3);

-- AlterTable
ALTER TABLE "vnpay_transaction" ALTER COLUMN "createdat" SET DATA TYPE TIMESTAMP(3),
ALTER COLUMN "updatedat" DROP DEFAULT,
ALTER COLUMN "updatedat" SET DATA TYPE TIMESTAMP(3);
