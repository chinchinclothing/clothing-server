import Joi from 'joi';

export default {
    create: Joi.object().keys({
        name: Joi.string().required(),
        code: Joi.string().required(),
        symbol: Joi.string().required(),
    }),
    getAll: Joi.object().keys({
        limit: Joi.number(),
        page: Joi.number(),
        value: Joi.string().allow(''),
        name: Joi.string().allow(''),
        code: Joi.string().allow(''),
    }),
    currencyId: Joi.object().keys({
        id: Joi.number().required()
    }),
    update: Joi.object().keys({
        name: Joi.string(),
        code: Joi.string(),
        symbol: Joi.string()
    }),
}