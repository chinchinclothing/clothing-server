import Joi from 'joi';
import locations from '.';

export default {
    deliveryLocation: Joi.object().keys({
        name: Joi.string().required(),
        address: Joi.string().required(),                                                       
        phone_number: Joi.string().required(),                                                  
        is_default: Joi.boolean(),                                                  
        state_location_id: Joi.number().required(),                                          
        city_location_id: Joi.number().required(),                                           
        district_location_id: Joi.number().required(),
    }),
    deliveryContactUpdate: Joi.object().keys({
        name: Joi.string(),
        address: Joi.string(),                                                       
        phone_number: Joi.string() ,                                                 
        is_default: Joi.boolean(),                                                  
        state_location_id: Joi.number(),                                          
        city_location_id: Joi.number(),                                           
        district_location_id: Joi.number(),
    }),
    deliveryId: Joi.object().keys({
        id: Joi.number().required(),
    }),
}

