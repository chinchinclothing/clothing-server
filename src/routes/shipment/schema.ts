import Joi from 'joi';
import locations from '.';

export default {
    shippingCost: Joi.object().keys({
        sender_info: Joi.object().keys({
            sender_detail_address: Joi.string().allow(''),
            sender_state: Joi.string().allow(''),
            sender_state_location_id: Joi.number().required(),
            sender_city: Joi.string().allow(''),
            sender_city_location_id: Joi.number(),
            sender_district: Joi.string().allow(''),
            sender_district_location_id: Joi.number()
        }),
        deliver_info: Joi.object().keys({
            deliver_detail_address: Joi.string().allow(''),
            deliver_state: Joi.string().allow(''),
            deliver_state_location_id: Joi.number().required(),
            deliver_city: Joi.string().allow(''),
            deliver_city_location_id: Joi.number(),
            deliver_district: Joi.string().allow(''),
            deliver_district_location_id: Joi.number()
        }),
        parcel_info: Joi.object().keys({
            parcel_weight: Joi.number().required()
        })
    }),
}

