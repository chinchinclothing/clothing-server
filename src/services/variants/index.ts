import {Prisma, variants as Variants} from '@prisma/client';
import prisma from '../../db/prisma/connection';

async function create(variant: Variants): Promise<Variants> {
    console.log("variant", variant)
    return await prisma.variants.create({
        data: variant
    });
}

async function findFirst(params:Prisma.variantsWhereInput): Promise<Variants | null> {
    try {
        console.log("params", params)
        return await prisma.variants.findFirst(
            { where: params }
        );
    } catch (error) {
        throw error;
    }
}

async function update(id: number, variant:Prisma.variantsUpdateInput): Promise<Variants | null> {
    try {
        return await prisma.variants.update({
            where: { id: id },
            data: variant
        });
    } catch (error) {
        throw error;
    }
}

async function findByPk(id: number): Promise<Variants | null> {
    try {
        return await prisma.variants.findUnique(
            { where: { id: id } }
        );
    } catch (error) {
        throw error;
    }
}


async function findByIdsDetails(ids: number[]){
    try {
        return await prisma.variants.findMany(
            { 
                where: { id: { in: ids } },
                include: {
                    prices: {
                        include: {
                            currency: true
                        }
                    },
                    products: {
                        select: {
                            id: true,
                            title: true,
                            url_img_default: true
                        }
                    },
                }
            }
        );
    } catch (error) {
        throw error;
    }
}

export default {
    create,
    findFirst,
    update,
    findByPk,
    findByIdsDetails,
    
}