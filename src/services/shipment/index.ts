import {Prisma} from '@prisma/client'
import prisma from '../../db/prisma/connection';

async function calculateShippingCost(weight:number, sendLocationId:number, deliveryLocationId:number): Promise<number> {
    console.log(weight, sendLocationId, deliveryLocationId);
    const sendLocation = await prisma.locations.findUnique({
        where: {
            id: sendLocationId
        }
    });

    const deliveryLocation = await prisma.locations.findUnique({
        where: {
            id: deliveryLocationId
        }
    });

    if (!sendLocation || !deliveryLocation) {
        throw new Error('Invalid location');
    }

    //same location
    if(sendLocation.id === deliveryLocation.id){
        console.log('Same location');
        return weight <= 1 ? 18000 : weight <= 1.5 ? 20500 : weight <= 2 ? 23000 : 23000 + Math.ceil((weight - 2) / 0.5) * 2500;
    }

    
    //same region
    if(sendLocation.region === deliveryLocation.region){
        console.log(27000,  Math.ceil((weight - 2) / 0.5) * 5000);
        return weight <= 1 ? 22000 : weight <= 1.5 ? 24500 : weight <= 2 ? 27000 : 27000 + Math.ceil((weight - 2) / 0.5) * 2500;
    }
    
    //special case
    const specialCase = ['Hà Nội', 'TP. Hồ Chí Minh', 'Đà Nẵng'];
    if(specialCase.includes(sendLocation.region) && specialCase.includes(deliveryLocation.region)){
        return weight <= 1 ? 22000 : weight <= 1.5 ? 27000 : weight <= 2 ? 30000 : 30000 + Math.ceil((weight - 2) / 0.5) * 5000;
    }
    
    //different region
    return weight <= 1 ? 22000 : weight <= 1.5 ? 27000 : weight <= 2 ? 30000 : 30000 + Math.ceil((weight - 2) / 0.5) * 5000;
}

export default {
    calculateShippingCost
}