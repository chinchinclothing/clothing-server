-- AlterTable
ALTER TABLE "users" ALTER COLUMN "status" SET DEFAULT 'active';

-- CreateTable
CREATE TABLE "keystore" (
    "id" SERIAL NOT NULL,
    "user_id" INTEGER NOT NULL,
    "primaryKey" TEXT NOT NULL,
    "secondaryKey" TEXT NOT NULL,
    "status" BOOLEAN DEFAULT true,
    "createdat" TIMESTAMP(3) DEFAULT CURRENT_TIMESTAMP,
    "updatedat" TIMESTAMP(3),

    CONSTRAINT "keystore_pkey" PRIMARY KEY ("id")
);
