import prisma from '../../db/prisma/connection';
import {Prisma} from '@prisma/client';


async function create (params: Prisma.currencyCreateInput) {
    try {
        const currency = await prisma.currency.create({
            data: params
        })
        return currency;
    } catch (error) {
        throw error
    }
}

async function update (id:number, params: Prisma.currencyUpdateInput) {
    try {
        const currency = await prisma.currency.update({
            where: {
                id  
            },
            data: params
        })
        return currency;
    } catch (error) {
        throw error
    }
}

async function findFirst (params: Prisma.currencyWhereInput) {
    try {
        const currency = await prisma.currency.findFirst(
            {
                where: params
            }
        )
        return currency;
    } catch (error) {
        throw error
    }
} 

async function findById (id: number) {
    try {
        const currency = await prisma.currency.findUnique({
            where: {
                id
            }
        })
        return currency;
    } catch (error) {
        throw error
    }
}

async function getAll (params: Prisma.currencyWhereInput,page: number = 1, limit:number = 10 ) {
    try {
        let where = {} as Prisma.currencyWhereInput;
        if(params.code){
            where.code = {
                contains: params.code as string,
                mode: 'insensitive'
            }
        }

        if(params.name){
            where.name = {
                contains: params.name as string,
                mode: 'insensitive'
            }
        }

        const totalCurrency = await prisma.currency.count({
            where
        });

        const take = limit ;
        const totalPages = Math.ceil(totalCurrency / take);

        if(page > totalPages){
            page = totalPages;
        }

          
        const skip = page>0 ? (page - 1) * take: 0;

        const currencies = await prisma.currency.findMany(
            {
                where,
                take,
                skip
            }
        )
        return {currencies, totalPages, currentPage:page};
    } catch (error) {
        throw error
    }
}

export default {
    create,
    findFirst,
    getAll,
    update,
    findById
} 
