* install redis server
```bash
    sudo apt install redis-server -y   
```

* uses systemd to manage running services
```bash
sudo apt install redis-server -y   
```
    * find `supervised` and replace by `supervised systemd`

* restart redis 
```bash
sudo systemctl restart redis
```

* check status
```bash
sudo systemctl status  redis
```

* config password
```bash
sudo nano /etc/redis/redis.conf
```
* uncommand and replace `foobared` by your password `# requirepass foobared`

