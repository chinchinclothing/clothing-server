import { SuccessMsgResponse } from './../../../core/ApiResponse';
import { Router } from "express";
import asyncHandler from "../../../helpers/asyncHandler";
import authorization from "../../../auth/authorization";
import authentication from "../../../auth/authentication";
import accessRoles from "../../../helpers/accessRoles";
import { e_role_code as RoleCode } from "@prisma/client";
import validator from "../../../helpers/validator";
import cartService from '../../../services/cart';
import cartItemsService from '../../../services/cart/cartItems';
import productServices from '../../../services/products'
import {SuccessResponse} from "../../../core/ApiResponse";
import { BadRequestError, InternalError } from "../../../core/ApiError";
import { ValidationSource } from "../../../helpers/validator";
import { ProtectedRequest } from "app-request";
import schema from "../schema";
import cart from "..";

const router  =  Router();

router.use(
    authentication,
    accessRoles(RoleCode.USER),
    authorization,
);

/**
 * 
 * Add item to shopping bag
 * 
 */
router.post(    
    '/',
    validator(schema.addCartItem),
    asyncHandler(
        async (req: ProtectedRequest, res)=>{
            //check cart exist
            let cartExist = await cartService.findCartByUserId(req.user.id, {status:'active'})
            
            if(!cartExist){
                //create cart
                cartExist = await cartService.createCart({user_id: req.user.id, status:'active'})
            }

            if(!cartExist){
                throw new InternalError("Try again")
            }

            //check product exist
            const productExist = await productServices.getProductPriceByVariantId(req.body.variant_id); 
            
            if(!productExist){
                throw new BadRequestError("Product or Variant not found");
            }

            //checkItem exist in cart 
            let itemExist = await cartItemsService.findCartItemActive(
                {
                    cart_id:cartExist.id, 
                    variant_id: req.body.variant_id
                }
            )
            
            if(!itemExist){
                //check quantity
                if(req.body.quantity > productExist.variants[0].stock){
                    throw new BadRequestError("Quantity not enough");
                }
                //add item to cart
                //add
                itemExist = await cartItemsService.create(
                    {
                        cart_id:cartExist.id,
                        variant_id: req.body.variant_id, 
                        quantity: req.body.quantity, 
                        status:'active',
                        price: productExist.variants[0].prices?.price as number,
                        compare_at_price: productExist.variants[0].prices?.compare_at_price as number
                    }
                )
            }else{
                //check quantity
                if(itemExist.quantity + req.body.quantity > productExist.variants[0].stock){
                    throw new BadRequestError("Quantity not enough");
                }
                //update
                itemExist = await cartItemsService.update(
                    itemExist.id,
                    {
                        quantity: itemExist.quantity + req.body.quantity,
                        price: productExist.variants[0].prices?.price,
                        compare_at_price: productExist.variants[0].prices?.compare_at_price||undefined
                    }
                )
            }
            
            //send response
            if(!itemExist){
                throw new InternalError("Try again");
            }

            return new SuccessResponse(
                'Add success',
                {cart_item:itemExist}
            ).send(res)
        }
    )
)


/**
 * 
 * Remove item out of shopping bag
 * 
 */
router.delete(
    '/items/:id',
    validator(schema.itemId, ValidationSource.PARAM),
    asyncHandler(
        async (req:ProtectedRequest, res)=>{
            const item = await cartItemsService.remove(parseInt(req.params.id));
            return new SuccessResponse(
                'Item had been removed',
                {}
            ).send(res)
        }
    )
)

/**
 * 
 * Retrieve list items of shopping bag
 *  
 */
router.get(
    '/items',
    asyncHandler(
        async (req:ProtectedRequest, res)=>{
            const userId = req.user.id;
            const items = await cartItemsService.findActiveCartItems(userId)
            return new SuccessResponse(
                'Success',
                {
                    cart_items:items
                }
            ).send(res)
        }
    )
)

export default router