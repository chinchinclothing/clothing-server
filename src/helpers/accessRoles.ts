import { RoleRequest} from 'app-request';
import {e_role_code as RoleCode} from '@prisma/client';
// import { RoleRequest, RoleCode} from 'app-request';
import { Response, NextFunction } from 'express';

export default (...roleCodes: RoleCode[]) =>
  (req: RoleRequest, res: Response, next: NextFunction) => {
    req.accessRoleCodes = roleCodes;
    next();
};
