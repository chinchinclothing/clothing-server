import Joi from "joi";
import {Prisma, e_collection_scope, e_collection_sort_order, e_highlightcollection_status} from '@prisma/client';

export default {
    createCollection: Joi.object().keys({
        title:Joi.string().required(),
        meta_title:Joi.string(),
        published_scope: Joi.string().valid(...Object.keys(e_collection_scope)),
        sort_order:Joi.string().valid(...Object.keys(e_collection_sort_order)),
        published:  Joi.boolean(),
        collection_image:Joi.array().items(
            Joi.object().keys({
                alt:Joi.string(),
                width:Joi.number(),
                height:Joi.number(),
                url:Joi.string().required()
            })
        ),
    }),
    
    collectionFilter: Joi.object().keys({
        title:Joi.string(),
        published_status:  Joi.boolean(),
        limit:Joi.number(),
        page:Joi.number(),
        featured:Joi.boolean(),
        published_scope: Joi.string().valid(...Object.keys(e_collection_scope)),
        created_at_min:Joi.date(),
        created_at_max:Joi.date(),
        updated_at_min:Joi.date(),
        updated_at_max:Joi.date(),
        published_at_min:Joi.date(),
        published_at_max:Joi.date(),
    }),
    
    collectionId: Joi.object().keys({
        id: Joi.number().required()
    }),
    updateCollection: Joi.object().keys({
        title:Joi.string(),
        meta_title:Joi.string(),
        published_scope: Joi.string().valid(...Object.keys(e_collection_scope)),
        sort_order:Joi.string().valid(...Object.keys(e_collection_sort_order)),
        published:  Joi.boolean(),
    }),
    collectionImageIdxs: Joi.object().keys({
        image_idxs:Joi.array().items(Joi.number().required())
    }),
    productIds: Joi.object().keys({
        product_ids:Joi.array().items(Joi.number().required()).required()
    })
}