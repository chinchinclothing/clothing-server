import { readFile } from 'fs';
import {Router} from 'express';
import authorization from '../../../auth/authorization';
import authentication from '../../../auth/authentication';
import { e_role_code as RoleCode } from '@prisma/client';
import accessRoles from '../../../helpers/accessRoles'
import validator ,{ValidationSource} from '../../../helpers/validator';
import asyncHandler from '../../../helpers/asyncHandler';
import {ProtectedRequest, PublicRequest} from 'app-request';
import * as userServices from '../../../services/users';
import orderServices from '../../../services/orders';
import paymentServices from '../../../services/payment';
import schema from '../schema';
import voucherServices from '../../../services/vouchers';
import { SuccessResponse } from '../../../core/ApiResponse';
import { BadRequestError } from '../../../core/ApiError';
import _ from 'lodash';
//================================================================
const userRouter = Router();
userRouter.use(
    authentication,
    accessRoles(RoleCode.USER),
    authorization,
);

//================================================================
const router = Router();
router.use('/', userRouter);
//================================================================
/**
 * 
 * create payment url
 * 
 */
userRouter.get(
    '/vnpay/create_payment_url',
    validator(schema.vnpayCreateURL, ValidationSource.QUERY),
    asyncHandler(async (req: ProtectedRequest, res) => {
        const orderId = parseInt(req.query.order_id as string);
        //check order payment status
        const order = await orderServices.getOrderById(orderId);
        const userId = req.user?.id;
        //if expired, create new vnpay transaction
        if (!order) throw new BadRequestError('Order not found');
        if (order.user_id !== userId) throw new BadRequestError('Order not found');
        if (order.payment_method !== 'VNPAY') throw new BadRequestError('Order not support VNPAY');
        if (order.payment_status=='PAID') throw new BadRequestError('Order has been paid');
        //check expired
        const expiredAt = order.expired_at as Date;
        if (expiredAt < new Date()) throw new BadRequestError('Order has been expired');
        
        const amount = order?.total_payment as number;
        const payment_url = await paymentServices.createVNPayUrlForOrder({amount, orderId});
        new SuccessResponse('Payment URL created successfully', {payment_url}).send(res);
    })
);
/**
 * 
 * Return payment req
 * 
 */
userRouter.get('/payment_return',

);

/**
 * 
 */
userRouter.get('/refund', );
userRouter.get('/IPN', 
    validator(schema.vnpayIPN, ValidationSource.QUERY),
    asyncHandler(async (req: ProtectedRequest, res) => {
        var vnp_Params = req.query;
        var secureHash = vnp_Params['vnp_SecureHash'];

        delete vnp_Params['vnp_SecureHash'];
        delete vnp_Params['vnp_SecureHashType'];

        vnp_Params = _.pick(vnp_Params, Object.keys(vnp_Params).sort());;
        var secretKey = process.env.vnp_HashSecret;
        var querystring = require('qs');
        var signData = querystring.stringify(vnp_Params, { encode: false });
        var crypto = require("crypto");
        var hmac = crypto.createHmac("sha512", secretKey);
        var signed = hmac.update(Buffer.from(signData, 'utf-8')).digest("hex");
        if (secureHash === signed) {
            var orderId = vnp_Params['vnp_TxnRef'];
            var rspCode = vnp_Params['vnp_ResponseCode'];
            if (rspCode === '00') {
                //Thanh toan thanh cong
                //update order status
                const updateData = _.pick(vnp_Params, ['vnp_TxnRef', 'vnp_PayDate','vnp_TransactionNo']) as any;
                const updateOrder = await orderServices.updateOrderVNPaymentSuccess(updateData);
                if (!updateOrder) {
                    return res.status(200).json({ RspCode: '97', Message: 'Fail checksum' })
                }
                console.log('updateOrder', updateOrder);
                return res.status(200).json({ RspCode: '00', Message: 'success' })
            }else{
                return res.status(200).json({ RspCode: '97', Message: 'Fail checksum' })
            }
        } else {
            return res.status(200).json({ RspCode: '97', Message: 'Fail checksum' })
        }
    })
);


userRouter.get('/hello', (req, res, next) => {
    res.send("Hello from payment");
})

export default router;