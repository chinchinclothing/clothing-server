# crawl data from shopee

import requests
import json
import time
import random

# if result.data.location_info.sub_location_info is empty, it means that it is the last level


def crawl_shopee_location(location_id):
    url = 'https://spx.vn/shipment/merchant/open/location/get_sub_location_layer_info?sub_level=1&location_id=' + str(location_id)
    print(url)
    print(location_id)
    headers = {
    }
    response = requests.get(url, headers=headers)
    result = json.loads(response.text)
    return result


def check_null_sub(varible):
    try:
        varible['data']['sub_location_info']
    except KeyError:
        print('KeyError')
        return True
    return False

i = 0
def recursive(location_id, level=0):
        if(level == 2):
            return
        data = crawl_shopee_location(location_id)
        # check is empty
        if check_null_sub(data):
            return
        else:
            parent_location = data['data']['location_info']
            for location in data['data']['sub_location_info']:
                # saveResult = format_location(location, parant_id, id)
                result = format_location(location,parent_location)
                saveResult = format_postgress_insert(result)
                saveLocation(saveResult, 'postgress')
                recursive(location['location_id'], result['level'])


def format_location(location,parent_location):
    name = location['name']
    if "'" in name:
        name = name.replace("'", "''")
    return {
        'id':               location['location_id'],
        'country':          location['country'],
        'name' :            name,
        'level':            location['level'],
        'longitude':        location['longitude'],
        'postcode':         location['postcode'],
        'latitude':         location['latitude'],
        'location_id':      location['location_id'],
        'parent_id':        parent_location['location_id'],
    }   

def format_postgress_insert(location):
    # id,shopee_location_id,country,name,level,longitude,latitude,parent_id
    return f"({location['id']},{location['location_id']},'{location['country']}','{location['name']}',{location['level']},{location['longitude']},{location['latitude']},{location['parent_id']}),"

def saveLocation(location, type='json'):
    # save on file json
    if type == 'json':
        with open('./location.json', 'a') as f:
            json.dump(location, f)
            f.write('\n')
    # save on file postgress
    elif type == 'postgress':
        with open('./location.sql', 'a') as f:
            # write location raw and add new line
            f.write(location)
            f.write('\n')

def main():
    location_id = -1
    recursive(location_id)

main()

