import Joi from 'joi';

export default {
    createCheckoutItem:Joi.object().keys({
        item_ids:Joi.array().items(
            Joi.number()
        )
    }),
    createCheckout:Joi.object().keys({
        shipping_address_id: Joi.number(),
        payment_method: Joi.string().valid('COD', 'VNPAY').required(),
        cart_items: Joi.array().items(
            Joi.object().keys({
                item_id: Joi.number().required(),
            })
        ).required(),
        coupon_code: Joi.string().allow(null, ''),
    })
}