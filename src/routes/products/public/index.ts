import {Router} from 'express';
import validator, { ValidationSource } from '../../../helpers/validator';
import schema from '../schema';
import asyncHandler from '../../../helpers/asyncHandler';
import { PublicRequest } from 'app-request';
import productsService from  '../../../services/products'
import { SuccessResponse } from '../../../core/ApiResponse';
import { valid } from 'joi';
import { BadRequestError } from '../../../core/ApiError';
//================================================================

const router = Router();

//================================================================

/**
 * 
 * Retrieve list of Products
 * 
 */
router.get(
    '/',
    validator(schema.productFilters, ValidationSource.QUERY),
    asyncHandler(
        async (req: PublicRequest, res)=>{
            req.query.status = 'active';
            const {products, totalPages, currentPage} = await productsService.findAll(req.query);
            return new SuccessResponse(
                'success',{
                    products,
                    meta:{
                        page:currentPage,
                        totalPage: totalPages
                    }
                }
            ).send(res);

        }
    )
)

/**
 * 
 * Retrieve a single product details
 * 
 */
router.get(
    '/:type/:value',
    validator(schema.productUniqueParams,ValidationSource.PARAM),
    asyncHandler(async(req:PublicRequest,res)=>{
        const product = await productsService.findByUnique(req.params.value,req.params.type);
        if(product?.status!=='active'){
            throw new BadRequestError("Products not found");
        }
        return new SuccessResponse('success',product).send(res);
    })
)
//================================================================

export default router;
