import Joi from 'joi';

export default {
    createReview: Joi.object().keys({
        rating: Joi.number().required(),
        comment: Joi.string().required()
    }),
    updateReview: Joi.object().keys({
        comment: Joi.string().required()
    }),
    id: Joi.object().keys({
        id: Joi.number().required()
    }),
}



