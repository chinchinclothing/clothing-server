import Joi from 'joi';

export default {
    vnpayCreateURL: Joi.object().keys({
        order_id: Joi.number().required(),
    }),
    vnpayIPN: Joi.object().keys({
        vnp_Amount:Joi.string().required(),
        vnp_BankCode:Joi.string().required(),
        vnp_BankTranNo:Joi.string(),
        vnp_CardType:   Joi.string().required(),
        vnp_OrderInfo: Joi.string().required(),
        vnp_PayDate: Joi.string(),
        vnp_ResponseCode: Joi.string().required(),
        vnp_TmnCode:Joi.string().required(),
        vnp_TransactionNo: Joi.string().required(),
        vnp_TransactionStatus:Joi.string().required(),
        vnp_TxnRef: Joi.string().required(),
        vnp_SecureHash: Joi.string().required(),
    })
}

