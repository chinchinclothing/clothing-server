import {Prisma, e_payment_method, e_order_status, e_cancelled_by} from '@prisma/client';
import prisma from '../../db/prisma/connection';
import _ from 'lodash';
import moment from 'moment';
interface AdminRetrieveListOrersFilter {
    limit?: number,
    page?: number,
    created_at_min?: Date,
    created_at_max?: Date,
    is_pair?: string|boolean,
    payment_method?: e_payment_method,
    status?: e_order_status,
    is_expired?: string|boolean,
    user_id?: number,
}

interface VNPaySuccessData {
    vnp_TxnRef: string,
    vnp_TransactionNo: string,
    vnp_PayDate: string
}

interface orderCancelData {
    orderId: number,
    userId: number,    
    reason: string,
}

async function create(order: any, orderItems: any, couponCode: string,  userId: number){
    try {
        
        const newOrder = await prisma.$transaction(
            async(tx)=>{
                const newOrder = await tx.orders.create({
                    data: {
                        ...order,
                        users:{
                            connect:{
                                id:userId
                            }
                        },
                        order_item:{
                            create: orderItems
                        }
                    },
                    include: {
                        order_item: true
                    }
                })

                //delete cart items
                const cartItems = await tx.cart_item.findMany({
                    where:{
                        carts:{
                            user_id:userId
                        },
                        variant_id:{
                            in: _.map(orderItems, (item)=>{return item.variant_id })
                        }
                    }
                });

                if(cartItems){
                    await tx.cart_item.deleteMany({
                        where:{
                            id:{
                                in: _.map(cartItems, (item)=>{return item.id})
                            }
                        }
                    })
                }
                //use coupon
                if(couponCode){
                    await tx.vouchers.update({
                        where:{
                            code:couponCode
                        },
                        data:{
                            current_uses:{
                                increment:1
                            },
                            voucher_usage_log:{
                                create:{
                                    user_id:userId,
                                    order_id:newOrder.id
                                }
                            }
                        }
                    });
                }

                for (const item of newOrder.order_item) {
                    const variantUpdate = await tx.variants.update({
                        where:{
                            id:item.variant_id
                        },
                        data:{
                            stock:{
                                decrement:item.quantity as number
                            },
                            inventory_transaction:{
                                create:{
                                    transaction_type:'out',
                                    quantity:item.quantity as number,
                                    purpose:'ORDER',
                                    description:'Order'
                                }
                            }
                        }
                    });
                    if(!variantUpdate){
                        throw new Error('Update stock failed');
                    }
                }
                return newOrder;
            }
        )
        return newOrder;
    } catch (error) {
        throw error;
    }
}  

async function getOrderById(orderId: number){
    try {
        const order = await prisma.orders.findUnique({
            where:{
                id:orderId
            }
        });
        return order;
    } catch (error) {
        throw error;
    }
}

async function getOrderDetail(orderId: number){
    try {
        const order = await prisma.orders.findUnique({
            where:{
                id:orderId
            },
            include:{
                order_item:{
                    include:{
                        variants:true
                    }
                },
                cancelled_orders:true,
            }
        });
        return order;
    } catch (error) {
        throw error;
    }
}

async function getOrderPaymentStatus(orderId:number){
    try {
        const order = await prisma.orders.findUnique({
            where:{
                id:orderId
            },
            include:{
                transactions:{
                    include:{
                        vnpay_transaction:true
                    }
                }
            }
        });
        return order;
    } catch (error) {
        throw error;
    }
}

async function updateOrderVNPaymentSuccess(data:VNPaySuccessData){
    try {
        console.log('data', data);
        const transaction = await prisma.transactions.findFirst({
            where:{
                vnpay_transaction:{
                    some:{
                        vnp_txn_ref:data.vnp_TxnRef,
                    }
                }
            },
            include:{
                vnpay_transaction:{
                    where:{
                        vnp_txn_ref:data.vnp_TxnRef,
                    }
                }
            }
        });
        console.log('transaction', transaction);
        if(!transaction){
            return;
        }
        
        const order = await prisma.orders.update({
            where:{
                id:transaction.order_id as number
            },
            data:{
                payment_status:'PAID',
                payment_at: moment(data.vnp_PayDate, "YYYYMMDDHHmmss").toDate(),
                status: 'WAITING_PICKUP',
                transactions:{
                    update:{
                        where:{
                            id:transaction.id
                        },
                        data:{
                            payment_status:'PAID',
                            vnpay_transaction:{
                                update:{
                                    where:{
                                        id:transaction.vnpay_transaction[0].id
                                    },
                                    data:{
                                        vnp_transaction_no:data.vnp_TransactionNo
                                    }
                                }
                            }
                        }
                    }
                }
            },
            include:{
                transactions:{
                    where:{
                        id:transaction.id
                    },
                    include:{
                        vnpay_transaction:{
                            where:{
                                id:transaction.vnpay_transaction[0].id
                            }
                        }
                    }
                }
            }
        });   

        return order;
    } catch (error) {
        throw error;
    }
}

async function adminRetrieveListOrers(filters:AdminRetrieveListOrersFilter ){
    try {
        let limit = filters.limit || 10;
        let page = filters.page || 1;
        const skip = (page - 1) * limit;
        const where: Prisma.ordersWhereInput = {
            AND:[
                filters.created_at_min?{created_at:{gte:filters?.created_at_min}}:{},
                filters.created_at_max?{created_at:{lte:filters?.created_at_max}}:{},
                filters.is_pair==='true'||filters.is_pair===true?{payment_at:{not:null}}:filters.is_pair==='false'||filters.is_pair===false?{payment_at:null}:{},
                filters.payment_method?{payment_method:filters.payment_method}:{},
                filters.is_expired==='true'||filters.is_expired===true?{expired_at:{lt:new Date()}, payment_at:null}:filters.is_expired==='false'||filters.is_expired===false?{OR:[{expired_at:{gte:new Date()}}, {payment_at:{not:null}}]}:{},
                filters.status?{status:filters.status}:{},
                filters.user_id?{user_id:filters.user_id}:{}
            ]
        };
        console.log('where', where);
        const count = await prisma.orders.count({
            where
        });
        const orders = await prisma.orders.findMany({
            where,
            take: limit,
            skip,
        });
        return {
            orders,
            page,
            totalPage: Math.ceil(count/limit)
        };
    } catch (error) {
        throw error;
    }
}

async function adminCancelOrder (data:orderCancelData){
    try {
        const order = await prisma.$transaction(
            async(tx)=>{
                const order = await prisma.orders.update({
                    where:{
                        id:data.orderId
                    },
                    data:{
                        status: 'CANCELLED',
                        cancelled_orders:{
                            create:{
                                canceled_by:'SELLER',
                                user_id:data.userId,
                                user_accepted_id:data.userId,
                                reason:data.reason
                            }
                        }  
                    },
                    include:{
                        order_item:true
                    }
                }) ; 
                for (const item of order.order_item) {
                    const variantUpdate = await tx.variants.update({
                        where:{
                            id:item.variant_id
                        },
                        data:{
                            stock:{
                                increment:item.quantity as number
                            },
                            inventory_transaction:{
                                create:{
                                    transaction_type:'in',
                                    quantity:item.quantity as number,
                                    purpose:'RETURN',
                                    description:'Cancel order'
                                }
                            }
                        }
                    });
                    if(!variantUpdate){
                        throw new Error('Update stock failed');
                    }
                }
                return order;
            }
        )
        return order;
    } catch (error) {
        throw error;
    }
}

interface OrderRequestData {
    orderId: number,
    userId: number,
    reason: string
}
async function requestOrder(data:OrderRequestData){
    try {
        const order = await prisma.orders.update({
            where:{
                id:data.orderId
            },
            data:{
                cancelled_orders:{
                    create:{
                        user_id:data.userId,
                        reason: data.reason,
                        status:'PENDING_APPROVAL',
                        canceled_by:'BUYER'
                    }
                }
            },
            select:{
                id:true,
                cancelled_orders:true
            }
        });
        return order;
    } catch (error) {
        throw error;
    }
}


async function updateOrderStatus(data:{orderId:number, userId:number, status:e_order_status}){
    try {
        const order = await prisma.orders.update({
            where:{
                id:data.orderId
            },
            data:{
                status:data.status
            }
        });
        return order;
    } catch (error) {
        throw error;
    }
}

async function getOrderItemById(id:number){
    try {
        const orderItems = await prisma.order_item.findFirst({
            where:{
                id
            },
            include:{
                variants:true
            }
        });
        return orderItems;
    } catch (error) {
        throw error;
    }
}

export default {
    create,
    getOrderById,
    getOrderDetail,
    getOrderPaymentStatus,
    updateOrderVNPaymentSuccess,
    requestOrder,
    getOrderItemById,
//================================================================
    adminRetrieveListOrers,
    adminCancelOrder,
    updateOrderStatus,
    
}