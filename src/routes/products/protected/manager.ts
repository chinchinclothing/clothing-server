import {Router} from 'express';
import authorization from '../../../auth/authorization';
import authentication from '../../../auth/authentication';
import { e_role_code as RoleCode } from '@prisma/client';
import accessRoles from '../../../helpers/accessRoles'
import validator ,{ValidationSource} from '../../../helpers/validator';
import schema from '../schema';
import asyncHandler from '../../../helpers/asyncHandler';
import {ProtectedRequest, PublicRequest} from 'app-request';
import productService from '../../../services/products';
import variantsServices from '../../../services/variants';
import colorsService from '../../../services/colors';
import sizesService from '../../../services/sizes';
import priceService from '../../../services/price';
import { SuccessResponse } from '../../../core/ApiResponse';
import { BadRequestError } from '../../../core/ApiError';


import _ from 'lodash';
import products from '..';
//================================================================

const router = Router();

//================================================================

router.use(
    authentication,
    accessRoles(RoleCode.ADMIN, RoleCode.MANAGER),
    authorization,
);

//================================================================
/**
 * videos routes
 */

//================================================================
router.post(
    '/:id/videos',
    validator(schema.productId, ValidationSource.PARAM),
    validator(schema.videosCreate),
    asyncHandler(async (req: ProtectedRequest, res) => {
        //check product exists
        const product = await productService.findByUnique(req.params.id, 'id');
        if (!product) throw new BadRequestError('Product not found');

        //add videos
        const videos = await productService.addProductVideos(parseInt(req.params.id), req.body.videos);
        new SuccessResponse('Videos added successfully', {}).send(res);
    })
)

/**
 * remove videos
 */
router.delete(
    '/:id/videos',
    validator(schema.productId, ValidationSource.PARAM),
    validator(schema.videosDelete),
    asyncHandler(async (req: ProtectedRequest, res) => {
         //check product exists
         const product = await productService.findByUnique(req.params.id, 'id');
         if (!product) throw new BadRequestError('Product not found');
 
         //remove videos
         const videos = await productService.removeProductVideos(parseInt(req.params.id), req.body.video_ids);
         new SuccessResponse('Videos removed successfully', {}).send(res);
    })
)

//================================================================
/**
 * images routes
 */
//================================================================
router.post(
    '/:id/images',
    validator(schema.productId, ValidationSource.PARAM),
    validator(schema.imagesCreate),
    asyncHandler(async (req: ProtectedRequest, res) => {
        //check product exists
        const product = await productService.findByUnique(req.params.id, 'id');
        if (!product) throw new BadRequestError('Product not found');

        //add images
        const images = await productService.addProductImages(parseInt(req.params.id), req.body.images);
        new SuccessResponse('Images added successfully', {}).send(res);
    })
)

/**
 * remove images
 */
router.delete(
    '/:id/images',
    validator(schema.productId, ValidationSource.PARAM),
    validator(schema.imagesDelete),
    asyncHandler(async (req: ProtectedRequest, res) => {
        //check product exists
        const product = await productService.findByUnique(req.params.id, 'id');
        if (!product) throw new BadRequestError('Product not found');

        //remove images
        const images = await productService.removeProductImages(parseInt(req.params.id), req.body.image_ids);
        new SuccessResponse('Images removed successfully', {}).send(res);
    })
)

//================================================================
/**
 * variant price routes
 * 
 */
//================================================================
router.post(
    '/variants/:variant_id/prices',
    validator(schema.variantId, ValidationSource.PARAM),
    validator(schema.variantPriceCreate),
    asyncHandler(async (req: ProtectedRequest, res) => {
        //check variant exists
        const variant = await variantsServices.findByPk(parseInt(req.params.variant_id)) as any;
        if (!variant) throw new BadRequestError('Variant not found');

        //check price exists
        const priceExist = await priceService.findFirst({variant_id: parseInt(req.params.variant_id)} as any);
        if (priceExist) throw new BadRequestError('Variant price already exists');
        
        //create price
        const price = await priceService.create({variant_id: parseInt(req.params.variant_id), ...req.body});

        //Update product price range
        await productService.updatePriceRange(parseInt(variant.product_id));
        new SuccessResponse('Created successfully', {price}).send(res);
    })
)

/**
 * Update a variant price
 */
router.put(
    '/variants/prices/:price_id',
    validator(schema.priceId, ValidationSource.PARAM),
    validator(schema.variantPriceUpdate),
    asyncHandler(async (req: ProtectedRequest, res) => {
        //check price exists
        const price = await priceService.findByPk(parseInt(req.params.price_id));
        if (!price) throw new BadRequestError('Price not found');
        //update price
        const updatedPrice = await priceService.update(parseInt(req.params.price_id), req.body) as any;

        //Update product price range
        if(updatedPrice){
            await productService.updatePriceRange(parseInt(updatedPrice.product_id));
        }

        new SuccessResponse('Updated successfully', {updatedPrice}).send(res);
    })
)

//================================================================
/**
 * variant routes
 */
//================================================================

/**
 * craete a variant
 */
router.post(
    '/:id/variants',
    validator(schema.productId, ValidationSource.PARAM),
    validator(schema.variantCreate),
    asyncHandler(async (req: ProtectedRequest, res) => {
        //check variant exists
        const variantExist = await variantsServices.findFirst({products:{id: parseInt(req.params.id)}, ...req.body});
        if (variantExist) throw new BadRequestError('Variant already exists');

        //check product exists
        const product = await productService.findByUnique(req.params.id, 'id');
        if (!product) throw new BadRequestError('Product not found');
        
        //check color exists
        const color = await colorsService.findByPk(req.body.color_id);
        if (!color) throw new BadRequestError('Color not found');

        //check size exists
        const size = await sizesService.findByPk(req.body.size_id);
        if (!size) throw new BadRequestError('Size not found');

        // Create the variant 
        const variant = await variantsServices.create({product_id: product.id, ...req.body});
        new SuccessResponse('Variant created successfully', {variant}).send(res);
    })
)

/**
 * update a variant by id
 */
router.put(
    '/:id/variants/:variant_id',
    validator(schema.variantIdUpdate, ValidationSource.PARAM),
    validator(schema.variantUpdate),
    asyncHandler(async (req: ProtectedRequest, res) => {
        //check variant exists
        const variantExist = await variantsServices.findFirst({product_id: req.params.id, id: req.params.variant_id} as any);
        if (!variantExist) throw new BadRequestError('Variant does not exist or not belong to the product');

        //check variant unique
        const variantUnique = await variantsServices.findFirst({product_id: parseInt(req.params.id), ...req.body} as any);
        if (variantUnique) throw new BadRequestError('Variant already exists');

        if (req.body.color_id) {
            //check color exists
            const color = await colorsService.findByPk(req.body.color_id);
            if (!color) throw new BadRequestError('Color not found');
        }
        
        if (req.body.size_id) {
            //check size exists
            const size = await sizesService.findByPk(req.body.size_id);
            if (!size) throw new BadRequestError('Size not found');
        }        

        // Create the variant 
        const variant = await variantsServices.update(parseInt(req.params.variant_id),req.body);
        new SuccessResponse('Variant created successfully', {variant}).send(res);
    })
)

/**
 * Retrieve a variant by id
 */
router.get(
    '/:variant_id',
    validator(schema.variantId, ValidationSource.PARAM),
    asyncHandler(async (req: ProtectedRequest, res) => {
        const variants = await variantsServices.findByPk(parseInt(req.params.variant_id));
        new SuccessResponse(
            'Success', 
            {variants}
        ).send(res);
    })
)

/**
 * Retrieve all variants
 */
router.get(
    '/:id/variants',
    validator(schema.productId, ValidationSource.PARAM),
    asyncHandler(async (req: ProtectedRequest, res) => {
        const variants = await productService.getVariantsOfProducts(parseInt(req.params.id));
        new SuccessResponse(
            'Success', 
            {variants}
        ).send(res);
    })
)

//================================================================
/**
 * product routes
 */
//================================================================
/**
 * create a product
 */
router.post(
    '/',
    validator(schema.productCreate),
    asyncHandler(async(req:ProtectedRequest,res)=>{
        try {
            //check slug 
            const existingProduct = await productService.findUniQueByOrParams(req.body);
            if(existingProduct){
                if(existingProduct.slug === req.body.slug){
                    throw new BadRequestError('Slug already exists');
                }
                if(existingProduct.sku === req.body.sku){
                    throw new BadRequestError('SKU already exists');
                }
            }
            //create
            const newProduct = await productService.create(req.body);
            return new SuccessResponse(
                "create success",
                _.pick(newProduct,[
                    'id','title','meta_title','slug','summary',
                    'sku','price','content','gender','quantity_sold',
                    'status', 'url_image_default'
                ])
            ).send(res)
        } catch (error) {
            throw error
        }
    })
)
//================================================================

/**
 * Retrieve all products
 */
router.get(
    '/',
    validator(schema.productFilters,ValidationSource.QUERY ),
    asyncHandler(async(req:ProtectedRequest,res)=>{
        const {products, totalPages, currentPage} = await productService.findAll(req.query);
        return new SuccessResponse(
            'success',{
                products,
                currentPage,
                totalPages
            }
        ).send(res);
    })
)
//================================================================
/**
 * Retrieve a single product
 */
router.get(
    '/:type/:value',
    validator(schema.productUniqueParams,ValidationSource.PARAM),
    asyncHandler(async(req:ProtectedRequest,res)=>{
        const product = await productService.findByUnique(req.params.value,req.params.type);
        return new SuccessResponse('success',product).send(res);
    })
)
//================================================================
/**
 * Retrieve a count of products
 */
router.get(
    '/count',
    validator(schema.productCountQueries,ValidationSource.QUERY),
    asyncHandler(async(req:ProtectedRequest,res)=>{
        const productCount = await productService.countProducts(req.query);
        return new SuccessResponse(
            'success',
            {
                count: productCount
            }
        ).send(res);
    })
)

//================================================================
/**
 * Update a product
 */
router.put(
    '/:id',
    validator(schema.productId, ValidationSource.PARAM ),
    validator(schema.productCreate, ValidationSource.BODY),
    asyncHandler(async(req:ProtectedRequest,res)=>{
        const existingProduct = await productService.findByUnique(req.params.id, 'id');
        
        if(!existingProduct){
            throw new BadRequestError('Product not found');
        }

        const updatedProduct = await productService.update(parseInt(req.params.id),req.body);
        return new SuccessResponse('update success',updatedProduct).send(res);
    })
)

export default router;
