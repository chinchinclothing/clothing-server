import {Router} from 'express';
import authorization from '../../../auth/authorization';
import authentication from '../../../auth/authentication';
import { e_role_code as RoleCode } from '@prisma/client';
import accessRoles from '../../../helpers/accessRoles'
import validator ,{ValidationSource} from '../../../helpers/validator';
import asyncHandler from '../../../helpers/asyncHandler';
import {ProtectedRequest, PublicRequest} from 'app-request';

import schema from '../schema';
import colorsService from '../../../services/colors';

import { SuccessResponse } from '../../../core/ApiResponse';
import { BadRequestError } from '../../../core/ApiError';

//================================================================

const router = Router();

//================================================================

router.post(
    '/',
    validator(schema.colorCreate),
    asyncHandler(async (req: ProtectedRequest, res) => {

        // Check if the color already exists
        const colorExists = await colorsService.findColor(req.body);
        if (colorExists) throw new BadRequestError('Color already exists');

        // Create the color 
        const color = await colorsService.create(req.body);
        new SuccessResponse('Color created successfully', {color}).send(res);
    })
)

router.get(
    '/',
    validator(schema.colorQuery, ValidationSource.QUERY),
    asyncHandler(async (req: ProtectedRequest, res) => {
        const colors = await colorsService.findAll(req.query);
        new SuccessResponse(
            'Colors found', 
            {colors}
        ).send(res);
    })
)

router.get(
    '/:id',
    validator(schema.colorId, ValidationSource.PARAM),
    asyncHandler(async (req: ProtectedRequest, res) => {
        const color = await colorsService.findByPk(parseInt(req.params.id));
        if (!color) throw new BadRequestError('Color not found');
        new SuccessResponse(
            'Color found', 
            {color}
        ).send(res);
    })
)

router.put(
    '/:id',
    validator(schema.colorId, ValidationSource.PARAM),
    validator(schema.colorUpdate),
    asyncHandler(async (req: ProtectedRequest, res) => {

        // Check if the color already exists
        const colorExists = await colorsService.findByPk(parseInt(req.params.id));
        if (!colorExists) throw new BadRequestError('Color not found');

        // Check if the color already exists
        const colorExists2 = await colorsService.findColor(req.body);
        if (
            colorExists2
            && colorExists2.id !== parseInt(req.params.id)
        ) throw new BadRequestError('Color already exists');

        // Update the color
        const color = await colorsService.update(parseInt(req.params.id), req.body);
        new SuccessResponse('Color created successfully', {color}).send(res);
    })
)



export default router;