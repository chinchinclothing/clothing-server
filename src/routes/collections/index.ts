import protectedRouter from './protected';
import publicRouter from './public';
import hilightProtectedRouter from './highlight/protected'
export default {
    protected:protectedRouter,
    public:publicRouter,
    highlight:{
        protected: hilightProtectedRouter
    }
}
