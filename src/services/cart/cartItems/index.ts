import { Prisma } from "@prisma/client";
import prisma from '../../../db/prisma/connection';

async function create (data: any){
    return await prisma.cart_item.create({
        data
    })
}

async function findCartItemActive(filters:Prisma.cart_itemWhereInput){
    return await prisma.cart_item.findFirst(
        {        
            where:{
                ...filters,
                carts:{
                    status:'active'
                }
            }
        }
    )

}

/**
 * 
 * Update item data 
 * 
 */
async function update(id:number, data: Prisma.cart_itemUncheckedUpdateInput){
   return await prisma.cart_item.update({
        where:{
            id
        },
        data
   })
};

/**
 * 
 * remove items
 * 
 */

async function remove(id:number){
    return await prisma.cart_item.delete(
        {
            where:{
                id
            }
        }
    )
}

async function findActiveCartItems(
    userId:number, 
){
    return await prisma.cart_item.findMany({
        where:{
            carts:{
                user_id:userId,
                status:'active'
            }
        },
        include:{
            variants:{
                include:{
                    sizes:true,
                    colors:true,
                    prices:{
                        include:{
                            currency:true
                        }
                    },
                    products:{
                        select:{
                            title:true,
                        },
                    },
                }
            }
        }
    })
}

async function findCartItemDetailsByManyIds (ids:number[]){
    return await prisma.cart_item.findMany({
        where:{
            id:{
                in:ids
            }
        },
        include:{
            variants:{
                include:{
                    sizes:true,
                    colors:true,
                    prices:{
                        include:{
                            currency:true
                        }
                    },
                    products:{
                        select:{
                            id:true,
                            title:true,
                        }
                    }
                }
            },
        }
    })
    
}

export default {
    findCartItemActive,
    update,
    create,
    remove,
    findActiveCartItems,
    findCartItemDetailsByManyIds
}