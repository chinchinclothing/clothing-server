import {Prisma, vouchers, user_promo} from '@prisma/client'
import prisma from '../../db/prisma/connection';

async function createVoucher(voucher: Prisma.vouchersCreateInput): Promise<vouchers> {
    return await prisma.vouchers.create({
        data: voucher
    });
}

async function updateVoucher(id: number, voucher: any): Promise<vouchers> {
    const {current_uses, ...voucherData} = voucher;
    return await prisma.vouchers.update({
        where: {id},
        data: {
            ...voucherData,
            ...(current_uses==='increment'?{current_uses: {increment : 1}}:current_uses==='decrement'?{current_uses: {decrement : 1}}:{})
        }
    });
}

async function addVoucherForUser(voucherId: number, userIds: number[]): Promise<void> {
    await prisma.vouchers.update({
        where: {id: voucherId},
        data:{
            user_promo:{
                create: [
                    ...userIds.map(user_id => ({user_id}))
                ]
            }
        }
    });
}

async function findVoucherExist(voucherId: number, userIds: number[]): Promise<user_promo[]> {
    return await prisma.user_promo.findMany({
        where:{
            user_id:{
                in:userIds
            },
            promo_id:voucherId
        }
    });
}

async function findUserCouponActiveByCode(userId: number, code:string): Promise<vouchers|null> {
    return await prisma.vouchers.findFirst({
        where:{
            code,
            start_at:{
                lte:new Date()
            },
            end_at:{
                gte:new Date()
            },
            OR:[
                {
                    user_promo:{
                        some:{
                            user_id:userId
                        }
                    }
                },
                {target_type:"public"}
            ]
        }
    });
}

async function findVouchersByFilters(filters: Prisma.vouchersWhereInput): Promise<vouchers[]> {
    return await prisma.vouchers.findMany({
        where: filters,
    });
}

async function findVoucherById(id: number): Promise<vouchers|null> {
    return await prisma.vouchers.findUnique({
        where: {id}
    });
}

async function updateVoucherUsageLog(userId: number, voucherId: number, orderId:number): Promise<void> {
    await prisma.vouchers.update({
        where:{
            id:voucherId
        },
        data:{
            current_uses:{
                increment:1
            },
            voucher_usage_log:{
                create:{
                    user_id:userId,
                    order_id:orderId
                }
            }
        }
    });
}

export default {
    createVoucher,
    updateVoucher,
    addVoucherForUser,
    findVoucherExist,
    findVouchersByFilters,
    findVoucherById,
    findUserCouponActiveByCode,
    updateVoucherUsageLog
}