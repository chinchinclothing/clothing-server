import {Router} from 'express';
import authorization from '../../../auth/authorization';
import authentication from '../../../auth/authentication';
import { e_role_code as RoleCode } from '@prisma/client';
import accessRoles from '../../../helpers/accessRoles'
import validator ,{ValidationSource} from '../../../helpers/validator';
import asyncHandler from '../../../helpers/asyncHandler';
import {ProtectedRequest, PublicRequest} from 'app-request';

import schema from '../schema';
import locationServices from '../../../services/locations';
import orderServices from '../../../services/orders';

import { SuccessResponse } from '../../../core/ApiResponse';
import { BadRequestError } from '../../../core/ApiError';
import _ from 'lodash';
//================================================================
const router = Router();
//================================================================

/**
 * 
 * check ipn
 * 
 */

router.get('/vnpay/IPN', 
    validator(schema.vnpayIPN, ValidationSource.QUERY),
    asyncHandler(async (req: ProtectedRequest, res) => {
        console.log(req.query);
        console.log("req.query.vnp_OrderInfo",req.query.vnp_OrderInfo)
        var vnp_Params = req.query;
        var secureHash = vnp_Params['vnp_SecureHash'];
        delete vnp_Params['vnp_SecureHash'];
        delete vnp_Params['vnp_SecureHashType'];
        vnp_Params['vnp_OrderInfo'] = encodeURIComponent(vnp_Params['vnp_OrderInfo'] as string).replace(/%20/g, '+');
        vnp_Params = _.pick(vnp_Params, Object.keys(vnp_Params).sort());;
        console.log('vnp_Params', vnp_Params);
        var secretKey = process.env.vnp_HashSecret;
        var querystring = require('qs');
        var signData = querystring.stringify(vnp_Params, { encode: false });
        console.log('signData', signData);
        var crypto = require("crypto");
        var hmac = crypto.createHmac("sha512", secretKey);
        var signed = hmac.update(Buffer.from(signData, 'utf-8')).digest("hex");
        console.log(signed);
        console.log(secureHash);
        if (secureHash === signed) {
            var orderId = vnp_Params['vnp_TxnRef'];
            var rspCode = vnp_Params['vnp_ResponseCode'];
            if (rspCode === '00') {
                //Thanh toan thanh cong
                //update order status
                const updateData = _.pick(vnp_Params, ['vnp_TxnRef', 'vnp_PayDate','vnp_TransactionNo']) as any;
                const updateOrder = await orderServices.updateOrderVNPaymentSuccess(updateData);
                if (!updateOrder) {
                    console.log('fail checksum 0');
                    return res.status(200).json({ RspCode: '97', Message: 'Fail checksum' })
                }
                console.log('updateOrder', updateOrder);
                return res.status(200).json({ RspCode: '00', Message: 'success' })
            }else{
                    console.log('fail checksum 1');
                    return res.status(200).json({ RspCode: '97', Message: 'Fail checksum' })
            }
        } else {
            console.log('fail checksum 2');
            return res.status(200).json({ RspCode: '97', Message: 'Fail checksum' })
        }
    })
);

export default router;