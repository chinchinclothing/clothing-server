
import { redis } from '../config';
import { createClient, RedisClientType } from 'redis';
import Logger from '../core/Logger';

const redisURL = `redis://:${redis.password}@${redis.host}:${redis.port}`;

class RedisClient {
  private static _instance: RedisClientType | null = null;

  private constructor() {
    // private constructor to prevent instantiation
  }

  public static getInstance(): RedisClientType {
    if (!RedisClient._instance) {
      RedisClient._instance = createClient({ url: redisURL });

      RedisClient._instance.on('connect', () => Logger.info('Cache is connecting'));
      RedisClient._instance.on('ready', () => Logger.info('Cache is ready'));
      RedisClient._instance.on('end', () => Logger.info('Cache disconnected'));
      RedisClient._instance.on('reconnecting', () => Logger.info('Cache is reconnecting'));
      RedisClient._instance.on('error', (e) => Logger.error(e));

      (async () => {
        await RedisClient._instance!.connect();
      })();

      // If the Node process ends, close the Cache connection
      process.on('SIGINT', async () => {
        if (RedisClient._instance) {
          await RedisClient._instance.disconnect();
        }
      });
    }
    return RedisClient._instance;
  }
}

const redisClient = RedisClient.getInstance();
export default redisClient;