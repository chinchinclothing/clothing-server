import {Router} from 'express';
import authorization from '../../../auth/authorization';
import authentication from '../../../auth/authentication';
import { e_role_code as RoleCode } from '@prisma/client';
import accessRoles from '../../../helpers/accessRoles'
import validator ,{ValidationSource} from '../../../helpers/validator';
import asyncHandler from '../../../helpers/asyncHandler';
import {ProtectedRequest, PublicRequest} from 'app-request';
import schema from '../schema';
import deliveryServices from '../../../services/deliveries';

import { SuccessResponse } from '../../../core/ApiResponse';
import { BadRequestError } from '../../../core/ApiError';
import _ from 'lodash';
//================================================================
const router = Router();
//================================================================
router.use(
    authentication,
    accessRoles(RoleCode.USER),
    authorization,
);
//================================================================
/**
 * 
 * add delivery location
 * 
 */
router.post(
    '/deliveries',
    validator(schema.deliveryLocation),
    asyncHandler(async (req: ProtectedRequest, res) => {
        const userId = req.user?.id;
        const data = {
            user_id: userId,
            ...req.body
        }
        //check delivery exists
        const delivery_exists = await deliveryServices.getDeliveryLocationsByFilter(
            {
                user_id: userId,
                phone_number: data.phone,
                state_location_id: data.state_location_id,
                city_location_id: data.city_location_id,
                district_location_id: data.district_location_id,
            }
        );

        if (delivery_exists) throw new SuccessResponse(
            'Delivery location already exists',
            {
                delivery_contact:delivery_exists
            }
        ).send(res)

        //check default
        if(data.is_default){
            const defaultLocation = await deliveryServices.setDefaultDeliveryLocationIsFalse(userId);
        }

        //check location
        const location = await deliveryServices.checkDeliveryIsValid(parseInt(data.state_location_id), parseInt(data.city_location_id), parseInt(data.district_location_id));
        console.log("location:",location);
        if (!location) throw new BadRequestError('Location is not valid');
        
        //create location
        const delivery = await deliveryServices.createContact(data);

        new SuccessResponse(
            'Location created successfully', 
            {
                delivery_contact:delivery
            }
        ).send(res);
    })
)

/**
 * 
 * get delivery locations
 * 
 */
router.get(
    '/deliveries',
    asyncHandler(async (req: ProtectedRequest, res) => {
        const userId = req.user?.id;
        const delivery_contacts = await deliveryServices.getDeliveryLocationsByUserId(userId);
        new SuccessResponse('Delivery locations', {
            delivery_contacts
        }).send(res);
    })
)

/**
 * 
 * get default delivery location
 * 
 */
router.get(
    '/deliveries/default',
    asyncHandler(async (req: ProtectedRequest, res) => {
        const userId = req.user?.id;
        const delivery_contact = await deliveryServices.getDefaultDeliveryLocationByUserId(userId);
        new SuccessResponse('Default delivery location', {
            delivery_contact
        }).send(res);
    })
)

/**
 * 
 * get delivery loction by id
 * 
 */
router.get(
    '/deliveries/:id',
    validator(schema.deliveryId, ValidationSource.PARAM),
    asyncHandler(async (req: ProtectedRequest, res) => {
        const id = parseInt(req.params.id);
        const delivery_contact = await deliveryServices.getDeliveryLocationsById(id);
        new SuccessResponse('Delivery contact', {
            delivery_contact
        }).send(res);
    })
)

/**
 * 
 * update delivery contact
 * 
 */
router.put(
    '/deliveries/:id',
    validator(schema.deliveryContactUpdate),
    validator(schema.deliveryId, ValidationSource.PARAM),
    asyncHandler(async (req: ProtectedRequest, res) => {
        const userId = req.user?.id;
        //check delivery exists
        const delivery_exists = await deliveryServices.getDeliveryLocationsById(parseInt(req.params.id));
        if (!delivery_exists) throw new BadRequestError('Delivery location not found');
        if (delivery_exists.user_id !== userId) throw new BadRequestError('Delivery location not found');

        const data = {
            ...req.body
        }

        //check default
        if(data.is_default){
            const defaultLocation = await deliveryServices.setDefaultDeliveryLocationIsFalse(userId);
        }

        //check location
        if(data.state_location_id || data.city_location_id || data.district_location_id){
            const location = await deliveryServices.checkDeliveryIsValid(parseInt(data.state_location_id), parseInt(data.city_location_id), parseInt(data.district_location_id));
            if (!location) throw new BadRequestError('Location is not valid');
        }

        const delivery = await deliveryServices.updateDeliveryContact(parseInt(req.params.id), data);

        new SuccessResponse('Delivery contact updated successfully', {
            delivery_contact:delivery
        }).send(res);
    })
)

/**
 * 
 * delete delivery contact
 * 
 */
router.delete(
    '/deliveries/:id',
    validator(schema.deliveryId, ValidationSource.PARAM),
    asyncHandler(async (req: ProtectedRequest, res) => {
        const userId = req.user?.id;
        //check delivery exists
        const delivery_exists = await deliveryServices.getDeliveryLocationsById(parseInt(req.params.id));
        if (!delivery_exists) throw new BadRequestError('Delivery location not found');
        if (delivery_exists.user_id !== userId) throw new BadRequestError('Delivery location not found');

        const delivery = await deliveryServices.removeDeliveryContact(parseInt(req.params.id));

        new SuccessResponse('Delivery contact deleted successfully', {
            delivery_contact:delivery
        }).send(res);
    })
)


//================================================================
export default router;