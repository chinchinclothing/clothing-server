import prisma from "../../db/prisma/connection";
import {e_role_code as RoleCode, users as User, Prisma } from '@prisma/client';

//==============================================================================

export async function findAllByManyId(ids:number[]):Promise<User[]>{
    try {
        const users = await prisma.users.findMany(
            {
                where:{
                    id:{
                        in:ids
                    }
                }
            }
        );
        return users;
    } catch (error) {
        throw error;
    }
}

export async function findById(id:number):Promise<User|null>{
    try {
        const user = await prisma.users.findFirst({
            where:{
                id:id
            }
        });
        return user;
    } catch (error) {
        throw error;
    }
}

export async function findByEmail(email:string):Promise<User|null>{
    try {
        const user = await prisma.users.findFirst({
            where:{
                email:email
            }
        });
        return user;
    } catch (error) {
        throw error;
    }   
}

export async function findByUsernameOrEmail(account:string):Promise<User|null>{
    try {
        const user = await prisma.users.findFirst({
            where:{
                OR:[
                    {
                        username:account
                    },
                    {
                        email:account
                    }
                ]
            }
        });
        return user;
    } catch (error) {
        throw error;
    }
}

export async function create(user:Prisma.usersCreateInput, role: RoleCode):Promise<User|null>{
    try {
        const result = await prisma.$transaction(
            async (tx)=>{

                const basicRole = await tx.roles.findFirst({
                    where:{
                        code:role
                    }
                });

                const newUser = await tx.users.create({
                    data:{
                        username:user.username,
                        email:user.email,
                        password:user.password,
                        full_name:user.full_name,
                        dob:user.dob&&new Date(user.dob),
                        address:user.address,
                        user_role:{
                            create:{
                                role_id:basicRole?.id
                            }
                        }
                    },
                });

                return newUser
            }
        )
        return result;
    } catch (error) {
        throw error;
    }
}

export async function findRolesByUserId(user_id:number):Promise<RoleCode[]>{
    try {
        const user = await prisma.users.findUnique({
            where: { id: user_id },
            include: {
                user_role: {
                    include: {
                        roles: true
                    }
                }
            }
        });
        const userRoles = await user?.user_role.map((ur) => ur?.roles?.code as RoleCode);
        return userRoles as RoleCode[];
    } catch (error) {
        throw error;
    }
}