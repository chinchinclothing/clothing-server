import { Router } from "express";
import asyncHandler from "../../../../helpers/asyncHandler";
import authorization from "../../../../auth/authorization";
import authentication from "../../../../auth/authentication";
import accessRoles from "../../../../helpers/accessRoles";
import { e_role_code as RoleCode } from "@prisma/client";
import validator from "../../../../helpers/validator";
import highlightService from "../../../../services/collections/highLights";
import collectionsService from "../../../../services/collections";
import {SuccessResponse} from "../../../../core/ApiResponse";
import { BadRequestError } from "../../../../core/ApiError";
import { ValidationSource } from "../../../../helpers/validator";
import { ProtectedRequest, PublicRequest } from "app-request";
import schema from "../schema";

const router = Router();

router.use(
    authentication,
    accessRoles(RoleCode.ADMIN, RoleCode.MANAGER),
    authorization,
);

router.post(
    '/',
    validator(schema.newHighlight),
    asyncHandler(
        async (req:ProtectedRequest, res)=>{
            //check collection exist
            const collection = await collectionsService.findByPk(parseInt(req.body.collection_id))
            if(!collection){
                throw new BadRequestError("Collection not found");
            }
            const newHighlight = await highlightService.create(req.body);
            new SuccessResponse(
                'Create success',
                {
                    highlight_collection: newHighlight
                }
            ).send(res)
        }
    )
)

router.put(
    '/:id',
    validator(schema.updateHighlight),
    asyncHandler(
        async (req: ProtectedRequest, res)=>{

            //check highlight exist
            const highlight = await highlightService.findByPk(parseInt(req.params.id));
            if(!highlight){
                throw new BadRequestError('highlight collection not found')
            }

            const updateHighlight = await highlightService.update(parseInt(req.params.id), req.body);

            return new SuccessResponse('Update success', {
                highlight_collection: updateHighlight
            }).send(res);
        }
    )
)

router.get(
    '/',
    validator(schema.filterHighlight, ValidationSource.QUERY),
    asyncHandler(
        async (req:ProtectedRequest, res)=>{
            console.log("hhuhuhuhu", req.query)

            const hightlights = await highlightService.findAll(req.query);
            return new SuccessResponse(
                'Success',
                {hightlight_collections:hightlights}
            ).send(res);
        }
    )
)

export default router;