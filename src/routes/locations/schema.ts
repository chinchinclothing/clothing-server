import Joi from 'joi';
import locations from '.';

export default {
    location: Joi.object().keys({
        country:Joi.string(),
        name: Joi.string().required(),
        level:  Joi.number().required(),
        longitude:  Joi.number(),
        postcode : Joi.string(),
        latitude:   Joi.number(),
        parent_id:  Joi.number(),
        shopee_location_id: Joi.number(),
    }),
    locations:  Joi.object().keys({
        locations: Joi.array().items(Joi.object().keys({
            country:Joi.string(),
            name: Joi.string().required(),
            level:  Joi.number().required(),
            longitude:  Joi.number(),
            postcode : Joi.string(),
            latitude:   Joi.number(),
            parent_id:  Joi.number(),
            shopee_location_id: Joi.number(),
        })).required()
    }),
    locationId: Joi.object().keys({
        location_id: Joi.number().required()
    }) ,
    masterLocation: Joi.object().keys({
        state_location_id: Joi.number().required(),
        city_location_id: Joi.number(),
        district_location_id: Joi.number(),
    }),
}

