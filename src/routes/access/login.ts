import {Router} from 'express';
import validator from '../../helpers/validator';
import schema from './schema';
import asyncHandler from '../../helpers/asyncHandler';
import { RoleRequest } from 'app-request';
import { BadRequestError,InternalError } from '../../core/ApiError';
import * as UserService from '../../services/users';
import * as KeystoreService from '../../services/keystore';
import { createTokens } from '../../auth/authUtils';
import { SuccessResponse } from '../../core/ApiResponse';
import { compare } from '../../auth/authUtils';
import crypto from 'crypto';
import _ from 'lodash';
//=========================================================

const router = Router();

//=========================================================

router.post('/', 
    validator(schema.login),
    asyncHandler(async (req:RoleRequest, res) => {
        //check user exist
        const user = await UserService.findByUsernameOrEmail(req.body.username);
        if(!user) throw new BadRequestError('User not registered');
        //compare password
        const isMatch = await compare(req.body.password,user.password);
        if(!isMatch) throw new BadRequestError('Invalid credentials');
        //create key
        const accessTokenKey = crypto.randomBytes(64).toString('hex');
        const refreshTokenKey = crypto.randomBytes(64).toString('hex');
        
        //save key
        await KeystoreService.create(user.id,accessTokenKey,refreshTokenKey);

        //create token
        const tokens = await createTokens(user,accessTokenKey,refreshTokenKey);

        //return success response
        new SuccessResponse(
            'Login Successful',
            {
                user:_.pick(user,['id','username','full_name','profile_pic_url']),
                tokens
            }
        ).send(res);
    })
)

export default router;