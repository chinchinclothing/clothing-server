import {Router} from 'express';
import managerRouter from './manager';
import authentication from '../../../auth/authentication';
import accessRoles from '../../../helpers/accessRoles';
import authorization from '../../../auth/authorization';
import { e_role_code as RoleCode } from '@prisma/client';

//================================================================

const router = Router();

//================================================================

const userRouter = Router();
userRouter.use(
    authentication,
    accessRoles(RoleCode.USER),
    authorization,
);
//================================================================
router.use('/users',userRouter);
router.use('/',managerRouter);

//================================================================

export default router;
