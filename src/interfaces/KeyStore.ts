export interface Keystore {
    id: number;
    user_id: number;
    primaryKey: string;
    secondaryKey: string;
    status: boolean|null;
    createdAt?: Date|null;
    updatedAt?: Date|null;
}