import {Router} from 'express';
import validator, {ValidationSource} from '../helpers/validator';
import asyncHandler from '../helpers/asyncHandler';
import schema from './schema';
import { getAccessToken, validateTokenData } from './authUtils';
import { ProtectedRequest } from 'app-request';
import JWT from '../core/JWT';
import * as UserService from '../services/users';
import * as KeystoreService from '../services/keystore';
import { AuthFailureError, AccessTokenError, TokenExpiredError } from '../core/ApiError';

//=========================================================
const router = Router();
//=========================================================
router.use(
    validator(schema.auth, ValidationSource.HEADER),
    asyncHandler(async (req:ProtectedRequest, res, next) => {
        //get access token from header
        req.accessToken = getAccessToken(req.headers.authorization);
        try{
            // validate token
            const payload = await JWT.validate(req.accessToken);
            // validate token data
            validateTokenData(payload);
            // find user by id
            const user = await UserService.findById(parseInt(payload.sub));
            // check user exist
            if(!user) throw new AuthFailureError('User not registered');
            req.user = user;
            const roles = await UserService.findRolesByUserId(user.id);
            req.user.roles = roles;
            console.log("user:",user);
            //find keystore
            const keystore = await KeystoreService.findKey(user.id,payload.prm);
            // keystore not found
            if(!keystore) throw new AuthFailureError('Invalid access token');

            return next();
        }catch(e){
            //catch jwt error
            if(e instanceof TokenExpiredError) throw new AccessTokenError(e.message);
            throw e;
        }
    }),
);
//=========================================================
export default router;