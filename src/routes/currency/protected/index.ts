import {e_role_code as RoleCode } from '@prisma/client';
import {Router} from 'express';
import authentication from '../../../auth/authentication';
import authorization from '../../../auth/authorization';
import accessRoles from '../../../helpers/accessRoles';
import validator, {ValidationSource} from '../../../helpers/validator';
import schema from '../schema';
import asyncHandler from '../../../helpers/asyncHandler';
import { ProtectedRequest } from 'app-request';
import { SuccessResponse } from '../../../core/ApiResponse';
import currencyService from '../../../services/currency';
import {BadRequestError} from '../../../core/ApiError';

const router = Router();

router.use(
    authentication,
    accessRoles(RoleCode.ADMIN, RoleCode.MANAGER),
    authorization
)

/**
 * Create a new currency
 */
router.post(
    '/',
    validator(schema.create),
    asyncHandler(async (req: ProtectedRequest, res) => {
        //check currency exist
        const currencyExist = await currencyService.findFirst({code:req.body.code,symbol:req.body.symbol});
        if(currencyExist) throw new BadRequestError('Currency already exist');
        //create currency
        const currency = await currencyService.create(req.body);
        new SuccessResponse('Create success', {currency}).send(res)
    })
)

/**
 * Get all currencies
 */

router.get(
    '/',
    validator(schema.getAll, ValidationSource.QUERY),
    asyncHandler(async (req: ProtectedRequest, res) => {
        const {page,limit, ...values} = req.query;
        const currencies = await currencyService.getAll(values, parseInt(page as string), parseInt(limit as string));
        new SuccessResponse('Success',{currencies}).send(res);
    })
)

/**
 * Update currency
 */
router.put(
    '/:id',
    validator(schema.currencyId, ValidationSource.PARAM),
    validator(schema.update),
    asyncHandler(async (req: ProtectedRequest, res) => {
        //check currency exist
        const currencyExist = await currencyService.findById(parseInt(req.params.id));
        if(!currencyExist) throw new BadRequestError('Currency not found');

        //check currency exist
        const currencyDuplicate = await currencyService.findFirst({code:req.body.code,symbol:req.body.symbol});
        if(currencyDuplicate && currencyDuplicate.id !== parseInt(req.params.id)) throw new BadRequestError('Currency already exist');

        //update currency
        const currency = await currencyService.update(parseInt(req.params.id), req.body);
        
        new SuccessResponse('Update success',{currency}).send(res);
    })
)


//==============================================================================

export default router;