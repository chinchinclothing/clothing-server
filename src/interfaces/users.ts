export default interface User {
    id:number,                 
    username:string,                
    email:string,                
    password:string,                
    full_name?:string|null,                
    dob?:Date|null,              
    address?:string | null,                
    phone_number?:string    | null,                
    status?:string | null,         
    createdat?:Date | null,               
    updatedat?:Date | null,             
}