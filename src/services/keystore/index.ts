import {Keystore} from "../../interfaces/KeyStore"
import prisma from "../../db/prisma/connection";
import { Prisma } from "@prisma/client";

export async function create(user_id:number, primaryKey:string, secondaryKey:string ):Promise<Keystore|null>{
    try {
        const newUser = await prisma.keystore.create({
            data:{
                user_id,
                primaryKey,
                secondaryKey,
            }
        });
        return newUser as Keystore;
    } catch (error) {
        throw error;
    }
}

export async function findKey(user_id:number, primaryKey:string):Promise<Keystore|null>{
    try {
        const keystore = await prisma.keystore.findFirst({
            where:{
                user_id,
                primaryKey,
                status:true
            }
        });
        return keystore as Keystore;
    } catch (error) {
        throw error;
    }
}

export async function find(
    user_id:number,
    primaryKey: string,
    secondaryKey: string
):Promise<Keystore|null>{
    try {
        const keystore = await prisma.keystore.findFirst({
            where:{
                user_id,
                primaryKey,
                secondaryKey
            }
        });
        return keystore as Keystore;
    } catch (error) {
        throw error;
    }
}

export async function remove(id:number):Promise<Keystore|null>{
    try {
        const keystore = await prisma.keystore.delete({
            where:{
                id
            }
        });
        return keystore as Keystore;
    } catch (error) {
        throw error;
    }
}