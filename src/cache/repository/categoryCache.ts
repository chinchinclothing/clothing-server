import { DynamicKey, getDynamicKey } from "../keys";
import { getJson, setJson } from "../query";
import { caching } from '../../config';
import { addMillisToCurrentDate } from '../../helpers/utils';
import {categories as Category} from '@prisma/client';
function getKeyForUnique<T>(uniqueKey: T) {
    let KeyType: DynamicKey;
    if(typeof uniqueKey === 'string'){
        KeyType = DynamicKey.CATEGORY_SLUG_UNIQUE;
    }else{
        KeyType = DynamicKey.CATEGORY_ID_UNIQUE;
    }   
    return getDynamicKey(KeyType, String(uniqueKey));
}

async function saveUniQue(category: Category, key: string){
    const typedKey = key as keyof Category;
    const uniqueKey = category[typedKey];
    return setJson(
        getKeyForUnique(uniqueKey),
        { ...category },
        addMillisToCurrentDate(caching.contentCacheDuration),
    );
}

async function fetchByUnique<T>(uniqueKey: T){
    const key = getKeyForUnique<T>(uniqueKey);
    console.log('key', key);
    return getJson<Category>(getKeyForUnique<T>(uniqueKey));
}

export default {
    saveUniQue,
    fetchByUnique
};