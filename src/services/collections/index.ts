import { min, some } from 'lodash';
import prisma from '../../db/prisma/connection';
import {Prisma, collections as Collections} from '@prisma/client';

async function create (collection:any):Promise<Collections|null> {
    let {collection_image,highlight_collections,...data} = collection;
    let collectionData = {
        ...data,
    } as Prisma.collectionsCreateInput;
    //check length of collection_image
    if (collection_image && collection_image.length > 0) {
        collectionData.collection_image = {
            create: collection_image as Prisma.collection_imageCreateInput
        }
    }
    if(highlight_collections && highlight_collections.length > 0){
        collectionData.highlight_collections = {
            create: highlight_collections as Prisma.highlight_collectionsCreateInput
        }
    }
    console.log("collectionData",collectionData);
    return await prisma.collections.create({
        data: collectionData
    });
}

async function findAll(query: any): Promise<any> {
  let where: Prisma.collectionsWhereInput = {};
  if (query.title) {
    where.title = {
      contains: String(query.title),
      mode: "insensitive",
    };
  }

  if (query.featured) {
    where.featured = {
      equals: query.featured,
    };
  }
  if (query.published_status === true || query.published_status === "true") {
    console.log("true huhuhu");
    where.published_at = {
      not: null,
    };
  }
  if (query.published_status === false || query.published_status === "false") {
    console.log("false rooiff");
    where.published_at = {
      equals: null,
    };
  }

  if (query.created_at_min && query.created_at_max) {
    where.created_at = {
      gte: new Date(query.created_at_min),
      lte: new Date(query.created_at_max),
    };
  } else {
    if (query.created_at_min) {
      where.created_at = {
        gte: new Date(query.created_at_min),
      };
    }
    if (query.created_at_max) {
      where.created_at = {
        lte: new Date(query.created_at_max),
      };
    }
  }

  if (query.published_at_min && query.published_at_max) {
    where.published_at = {
      gte: new Date(query.published_at_min),
      lte: new Date(query.published_at_max),
    };
  } else {
    if (query.published_at_min) {
      where.published_at = {
        gte: new Date(query.published_at_min),
      };
    }
    if (query.published_at_max) {
      where.published_at = {
        lte: new Date(query.published_at_max),
      };
    }
  }

  if (query.updated_at_min && query.updated_at_max) {
    where.updated_at = {
      gte: new Date(query.updated_at_min),
      lte: new Date(query.updated_at_max),
    };
  } else {
    if (query.updated_at_min) {
      where.updated_at = {
        gte: new Date(query.updated_at_min),
      };
    }
    if (query.updated_at_max) {
      where.updated_at = {
        lte: new Date(query.updated_at_max),
      };
    }
  }

  if (query.published_scope) {
    const OR = [
      {
        published_scope: "all",
      },
    ] as Prisma.collectionsWhereInput[];

    if (query.published_scope === "all") {
      OR.push({
        published_scope: "web",
      });
      OR.push({
        published_scope: "mobile",
      });
    } else {
      OR.push({
        published_scope: query.published_scope,
      });
    }

    where.OR = OR;
  }

  const take = parseInt(query.limit ? query.limit : 10);
  const skip = query.page ? (query.page - 1) * take : 0;

  console.log(where);

  const count = await prisma.collections.count({ where });

  const collections = await prisma.collections.findMany({
    where,
    take: take,
    skip,
  });
  let totalPage = Math.ceil(count / take);
  let page = query.page ? query.page : 1;
  if (page > totalPage) {
    page = totalPage;
  }
  return { collections, page, totalPage };
}

async function findByPk(id: number): Promise<Collections | null> {
  return await prisma.collections.findUnique({
    where: {
      id,
    },
  });
}

async function update(
  id: number,
  collection: any
): Promise<Collections | null> {
  let { collection_image, ...data } = collection;
  let collectionData = {
    ...data,
  } as Prisma.collectionsUpdateInput;

  return await prisma.collections.update({
    where: {
      id,
    },
    data: collectionData,
  });
}

async function deleteImages(
  id: number,
  imageIdxs: Array<number>
): Promise<Collections | null> {
  return await prisma.collections.update({
    where: {
      id,
    },
    data: {
      collection_image: {
        deleteMany: {
          id: {
            in: imageIdxs,
          },
        },
      },
    },
  });
}

async function addProducts(
  collection_id: number,
  params: any
): Promise<Collections | null> {
  return await prisma.collections.update({
    where: {
      id: collection_id,
    },
    data: {
      product_collection: {
        create: params.map((param: any) => {
          return {
            products: {
              connect: {
                id: param,
              },
            },
          };
        }),
      },
    },
  });
}

async function removeProducts(
  collection_id: number,
  params: any
): Promise<Collections | null> {
  return await prisma.collections.update({
    where: {
      id: collection_id,
    },
    data: {
      product_collection: {
        deleteMany: {
          product_id: {
            in: params,
          },
        },
      },
    },
  });
}

async function findHighLightCollection(params: any): Promise<any> {
  let where: Prisma.collectionsWhereInput = {};

  if (params.published_scope) {
    const OR = [
      {
        published_scope: "all",
      },
    ] as Prisma.collectionsWhereInput[];

    if (params.published_scope === "all") {
      OR.push({
        published_scope: "web",
      });
      OR.push({
        published_scope: "mobile",
      });
    } else {
      OR.push({
        published_scope: params.published_scope,
      });
    }

    where.OR = OR;
  }

  if (params.published_at_min && params.published_at_max) {
    where.published_at = {
      gte: new Date(params.published_at_min),
      lte: new Date(params.published_at_max),
    };
  } else {
    if (params.published_at_min) {
      where.published_at = {
        gte: new Date(params.published_at_min),
      };
    }
    if (params.published_at_max) {
      where.published_at = {
        lte: new Date(params.published_at_max),
      };
    }
  }

  if (params.created_at_min && params.created_at_max) {
    where.created_at = {
      gte: new Date(params.created_at_min),
      lte: new Date(params.created_at_max),
    };
  } else {
    if (params.created_at_min) {
      where.created_at = {
        gte: new Date(params.created_at_min),
      };
    }
    if (params.created_at_max) {
      where.created_at = {
        lte: new Date(params.created_at_max),
      };
    }
  }

  return await prisma.collections.findMany({
    where: {
      ...where,
      highlight_collections: {
        some: {
          published_at: {
            not: null,
          },
          status: {
            equals: "active",
          },
        },
      },
    },
    include: {
      highlight_collections: true,
      collection_image: {
        where: {
          type: "banner",
        },
      },
    },
  });
}

/**
 * 
 * retrieve list product of collection
 * 
 */

async function findProductsOfCollection (collection_id:number, filter:any):Promise<any> {
    // get filter
    let {limit, page, color_id, size_id, gender, min_price, max_price, sort_order } = filter;

    // where
    let where = {
        product_collection:{
            some:{
                collection_id
            }
        },
        AND:[
            color_id?{variants:{some:{color_id:parseInt(color_id)}}}:{},
            size_id?{variants:{some:{size_id:parseInt(size_id)}}}:{},
            gender?{gender}:{},
            min_price && max_price?{prices: {
                some: {
                  price: {
                    gte:parseFloat(min_price),
                    lte: parseFloat(max_price),
                  },
                },
              }, }:{},
        ]
    }   

    //order by
    let orderBy = {};
    switch (sort_order) {
        case 'newest':
          orderBy = { createdat: 'desc' };
          break;
        case 'high-to-low':
          orderBy = { min_price: 'desc' };
          break;
        case 'low-to-high':
          orderBy = { max_price: 'asc' };
          break;
        case 'best-seller':
          orderBy = { quantity_sold: 'desc' };
          break;
        default:
          orderBy = { createdat: 'desc' };
      }


    // pagination
    const count = await prisma.products.count({
        where
    })
    const totalPage =  Math.ceil(count/limit)
    page = page>totalPage?totalPage:page;
    const skip = page ? (page - 1) * limit : 0;

    console.log("order by",orderBy);
    // get products
    const products = await prisma.products.findMany({
        where,
        orderBy,
        include:{
            variants:{
                include:{
                    colors:true
                }
            },
        },
        take: parseInt(limit),
        skip
    })

    return {products, totalPage, page};
}


//=======================================================

export default {
    create,
    findAll,
    findByPk,
    update,
    deleteImages,
    addProducts,
    removeProducts,
    findHighLightCollection,
    findProductsOfCollection
}
