-- AlterTable
ALTER TABLE "categories" ALTER COLUMN "featured" SET DEFAULT false;

-- AlterTable
ALTER TABLE "users" ALTER COLUMN "status" SET DEFAULT 'active';
