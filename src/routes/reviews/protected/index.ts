import {Router} from 'express';
import authorization from '../../../auth/authorization';
import authentication from '../../../auth/authentication';
import { e_role_code as RoleCode } from '@prisma/client';
import accessRoles from '../../../helpers/accessRoles'
import validator ,{ValidationSource} from '../../../helpers/validator';
import asyncHandler from '../../../helpers/asyncHandler';
import {ProtectedRequest, PublicRequest} from 'app-request';
import schema from '../schema';
import orderServices from '../../../services/orders';
import productServices from '../../../services/products';

import { SuccessResponse } from '../../../core/ApiResponse';
import { BadRequestError } from '../../../core/ApiError';
import _ from 'lodash';
//================================================================
const userRouter = Router();
userRouter.use(
    authentication,
    accessRoles(RoleCode.USER),
    authorization,
);
//================================================================
const router = Router();
router.use('/', userRouter);
//================================================================
/**
 * 
 * Review order
 * 
 */

userRouter.post(
    '/orders/items/:id',
    validator(schema.id, ValidationSource.PARAM),
    validator(schema.createReview),
    asyncHandler(async (req: ProtectedRequest, res) => {
        const orderItemId = parseInt(req.params.id);
        const orderItem = await orderServices.getOrderItemById(orderItemId);
        if(!orderItem) throw new BadRequestError('Order item not found');        
        if(orderItem.id !== orderItemId) throw new BadRequestError('Order item not found');
        const orderId = orderItem.order_id as number;
        const order = await orderServices.getOrderDetail(orderId);
        if (order?.status !== 'COMPLETED') throw new BadRequestError('Order is not completed');
        if(!order?.user_id || order?.user_id !== req.user.id) throw new BadRequestError('Order not found');
        const reviewIsExist = await productServices.getReviewByOrderItemId(orderItemId);
        if(reviewIsExist) throw new BadRequestError('Review is exist for this product');
        const reviewData = {
            orderItemId: orderItemId,
            userId: req.user.id,
            rating: req.body.rating,
            comment: req.body.comment
        }
        const review = await productServices.createReview(reviewData);
        if(!review) throw new BadRequestError('Review fail');
        new SuccessResponse('Review order success', {
            review:{
                ...review,
                variants: orderItem.variants,
            }
        }).send(res);
    })
)

/**
 * 
 * Update review
 * 
 */
userRouter.post(
    '/:id',
    validator(schema.id, ValidationSource.PARAM),
    validator(schema.updateReview),
    asyncHandler(async (req: ProtectedRequest, res) => {
        const reviewIsExist = await productServices.getReviewById(parseInt(req.params.id));
        const order = await orderServices.getOrderDetail(parseInt(req.params.id));
        if(!reviewIsExist) throw new BadRequestError('Review is exist for this product');
        const review = await productServices.updateReview(parseInt(req.params.id), req.body.comment);
        if(!review) throw new BadRequestError('Review fail');
        new SuccessResponse('Review order success', {
            review
        }).send(res);
    })
)
//================================================================
export default router;


