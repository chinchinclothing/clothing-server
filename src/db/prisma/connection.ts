import { PrismaClient } from '@prisma/client'

class Prisma extends PrismaClient {
  async onModuleInit() {
    await this.$connect();
  }

  async onModuleDestroy() {
    await this.$disconnect();
  }
}

const prisma = new Prisma({log: ['query', 'info', 'warn', 'error']});
export default prisma;