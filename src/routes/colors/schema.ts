import Joi from "joi";

export default {
    colorCreate: Joi.object().keys({
        color: Joi.string().required(),
        hex: Joi.string().length(6).required()
    }),
    colorId: Joi.object().keys({
        id: Joi.string().required()
    }),
    colorQuery: Joi.object().keys({
        color: Joi.string()
    }),
    colorUpdate: Joi.object().keys({
        color: Joi.string(),
        hex: Joi.string().length(6)
    }),
}