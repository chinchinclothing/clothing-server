import Joi from "joi";

export default {
    sizeCreate: Joi.object().keys({
        size: Joi.string().required(),
    }),
    sizeId: Joi.object().keys({
        id: Joi.string().required()
    }),
    sizeQuery: Joi.object().keys({
        size: Joi.string()
    }),
    sizeUpdate: Joi.object().keys({
        size: Joi.string(),
    }),
}