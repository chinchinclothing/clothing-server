import express from 'express';
import { ForbiddenError } from '../core/ApiError';
import { PublicRequest } from '../types/app-request';
import schema from './schema'
import validator, { ValidationSource } from '../helpers/validator';
import asyncHandler from '../helpers/asyncHandler';
import { Header } from '../core/utils';

const router = express.Router();

export default router.use(
  validator(schema.apiKey, ValidationSource.HEADER),
  asyncHandler(async (req: PublicRequest, res, next) => {
    const key = req.headers[Header.API_KEY]?.toString();
    if (!key) throw new ForbiddenError();

    // const apiKey = await ApiKeyRepo.findByKey(key);
    const apiKey = key === 'GCMUDiuY5a7WvyUNt9n3QztToSHzK7Uj'?{
      id: 30,
      key: 'hihihihihi',
      version: 1,
      permissions: [],
      comments: [],
      status: true,
    }:null;

    if (!apiKey) throw new ForbiddenError();

    req.apiKey = apiKey;

    return next();
  }),
);
