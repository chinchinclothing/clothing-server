import {Prisma} from '@prisma/client'
import prisma from '../../db/prisma/connection';
import _ from 'lodash';
import moment from 'moment';
import axios from 'axios';

//create 
interface PaymentInfor {
    amount: number, 
    orderId: number
}

async function createVNPayUrlForOrder(paymentInfor: PaymentInfor){
    try {
            
                // var ipAddr = req.headers['x-forwarded-for'] ||
                //     req.connection.remoteAddress ||
                //     req.socket.remoteAddress ||
                //     req.connection.socket.remoteAddress;

        
                let tmnCode = process.env.vnp_TmnCode;
                let secretKey = process.env.vnp_HashSecret;
                let vnpUrl = process.env.vnp_Url;
                let returnUrl = process.env.vnp_ReturnUrl||"";
                const orderExpiredSetting = await prisma.system_settings.findFirst
                ({
                    where: {
                        key: 'order_expired_time'
                    }
                });

                const expireMinutes = orderExpiredSetting?.value as string||'10';
        
                // Lấy ngày hiện tại
                let currentDate = new Date();
                // Thêm 5 phút vào ngày hiện tại
                let newDate = new Date(currentDate.getTime());
                //format: yyyyMMddHHmmss
                let expireDate =  moment(newDate).add(parseInt(expireMinutes), 'minutes').utcOffset(7).format("YYYYMMDDHHmmss");
                let createDate = moment(newDate).utcOffset(7).format("YYYYMMDDHHmmss");
                let uniqueId = moment().utcOffset(7).format("YYYYMMDDHHmmss");
                let amount = paymentInfor.amount;
                // let bankCode = paymentInfor.bankCode;
                let bankCode = '';
        
                let orderInfo = `Thanh toan hoa hoa don, id don hang ${paymentInfor.orderId}`;
                // let orderType = paymentInfor.orderType;
                let orderType = 'topup';
        
                // let locale = paymentInfor.language;
                let locale = 'en';
                if (locale === null || locale === '') {
                    locale = 'vn';
                }
                let currCode = 'VND';
                let vnp_Params = {} as any;
                vnp_Params['vnp_Version'] = '2.1.0';
                vnp_Params['vnp_Command'] = 'pay';
                vnp_Params['vnp_TmnCode'] = tmnCode;
                // vnp_Params['vnp_Merchant'] = 'ridewizard'
                vnp_Params['vnp_Locale'] = locale;
                vnp_Params['vnp_CurrCode'] = currCode;
                vnp_Params['vnp_TxnRef'] = uniqueId+paymentInfor.orderId;
                vnp_Params['vnp_OrderInfo'] = encodeURIComponent(orderInfo).replace(/%20/g, '+');;
                vnp_Params['vnp_OrderType'] = orderType;
                vnp_Params['vnp_Amount'] = amount * 100;
                vnp_Params['vnp_ReturnUrl'] = encodeURIComponent(returnUrl);;
                vnp_Params['vnp_IpAddr'] = '127.0.0.1';
                vnp_Params['vnp_CreateDate'] = createDate;
                vnp_Params['vnp_ExpireDate'] = expireDate;
                if (bankCode !== null && bankCode !== '') {
                    vnp_Params['vnp_BankCode'] = bankCode;
                }
                vnp_Params =  _.pick(vnp_Params, Object.keys(vnp_Params).sort());
                //lowercase key and change value to string
                const vnpParamsSave ={
                    vnp_command: vnp_Params['vnp_Command'], 
                    vnp_txn_ref:  vnp_Params['vnp_TxnRef'],
                    vnp_order_info: vnp_Params['vnp_OrderInfo'],
                    vnp_transaction_date: vnp_Params['vnp_CreateDate'],
                }
                //save vnpay data
                const vnpayData = await prisma.transactions.create({
                    data: {
                        orders:{
                            connect:{
                                id:paymentInfor.orderId,
                            }
                        },
                        amount: amount,
                        payment_status: 'PENDING',
                        payment_method: 'VNPAY',
                        expired_at: moment(newDate).add(parseInt(expireMinutes), 'minutes').toDate(),
                        vnpay_transaction:{
                            create:{
                                ...vnpParamsSave 
                            }
                        }
                    },
                    include:{
                        vnpay_transaction:true
                    }
                })

                if(!vnpayData){
                    throw new Error('Cannot create vnpay data');
                }

                let querystring = require('qs');
                let signData = querystring.stringify(vnp_Params, { encode: false });
                let crypto = require("crypto");
                let hmac = crypto.createHmac("sha512", secretKey);
                let signed = hmac.update(Buffer.from(signData, 'utf-8')).digest("hex");
                vnp_Params['vnp_SecureHash'] = signed;
                vnpUrl += '?' + querystring.stringify(vnp_Params, { encode: false });
                return vnpUrl;
    } catch (error) {
        throw error;
    }
}  

async function refundVNPayTransaction(orderId: number, userId: number){
    try {
         //get payment transaction 
         const paymentTransaction =  await prisma.orders.findUnique({
            where:{
                id:orderId
            },
            include:{
                transactions:{
                    include:{
                        vnpay_transaction:true
                    }
                }
            }
        });

         let date = new Date();
 
         let crypto = require("crypto");
 
         let vnp_TmnCode = process.env.vnp_TmnCode;
         let secretKey = process.env.vnp_HashSecret;
         let vnp_Api = process.env.vnp_Api;
 
 
         let vnp_TxnRef = paymentTransaction?.transactions[0].vnpay_transaction[0].vnp_txn_ref;
 
         // let vnp_TransactionDate = req.body.transDate; //
         let vnp_TransactionDate = paymentTransaction?.transactions[0].vnpay_transaction[0].vnp_transaction_date;
         let vnp_Amount = paymentTransaction?.transactions[0].amount as number * 100;
         let vnp_TransactionType = '02';
         let vnp_CreateBy = userId;
 
         let currCode = 'VND';
 
         let vnp_RequestId = moment().utcOffset(7).format("YYYYMMDDHHmmss")+orderId;
         let vnp_Version = '2.1.0';
         let vnp_Command = 'refund';
         let vnp_OrderInfo = 'Hoan tien GD ma:' + vnp_TxnRef;
 
         let vnp_IpAddr = '127.0.0.1';
 
         let vnp_CreateDate = moment(date).format('YYYYMMDDHHmmss');
 
         let vnp_TransactionNo = '0';
 
         let data = vnp_RequestId + "|" + vnp_Version + "|" + vnp_Command + "|" + vnp_TmnCode + "|" + vnp_TransactionType + "|" + vnp_TxnRef + "|" + vnp_Amount + "|" + vnp_TransactionNo + "|" + vnp_TransactionDate + "|" + vnp_CreateBy + "|" + vnp_CreateDate + "|" + vnp_IpAddr + "|" + vnp_OrderInfo;
         let hmac = crypto.createHmac("sha512", secretKey);
         let vnp_SecureHash = hmac.update(Buffer.from(data, 'utf-8')).digest("hex");
         let dataObj = {
             'vnp_RequestId': vnp_RequestId,
             'vnp_Version': vnp_Version,
             'vnp_Command': vnp_Command,
             'vnp_TmnCode': vnp_TmnCode,
             'vnp_TransactionType': vnp_TransactionType,
             'vnp_TxnRef': vnp_TxnRef,
             'vnp_Amount': vnp_Amount,
             'vnp_TransactionNo': vnp_TransactionNo,
             'vnp_CreateBy': `${vnp_CreateBy}`,
             'vnp_OrderInfo': vnp_OrderInfo,
             'vnp_TransactionDate': vnp_TransactionDate,
             'vnp_CreateDate': vnp_CreateDate,
             'vnp_IpAddr': vnp_IpAddr,
             'vnp_SecureHash': vnp_SecureHash
         };

        const refundResult = await axios({
            method: 'post',
            url: vnp_Api,
            data: dataObj
        });
       
        if (refundResult?.data.vnp_ResponseCode !== '00') {
            return
        }

        const updateOrderRefund = await prisma.orders.update({
            where:{
                id:orderId
            },
            data:{
                payment_status: 'REFUND',
                transactions:{
                    create:{
                        payment_status: 'REFUND',
                        payment_method: 'VNPAY',
                        amount: parseFloat(refundResult.data.vnp_Amount)/100,
                        vnpay_transaction:{
                            create:{
                                vnp_txn_ref: refundResult.data.vnp_TxnRef,
                                vnp_order_info:refundResult.data.vnp_OrderInfo,
                                vnp_transaction_date:refundResult.data.vnp_PayDate,
                                vnp_transaction_no:refundResult.data.vnp_TransactionNo,
                                vnp_command: refundResult.data.vnp_Command,
                            }
                        }
                    }
                }
            },
            include:{
                transactions:{
                    where:{
                        payment_method: 'VNPAY',
                        vnpay_transaction:{
                            some:{
                                vnp_txn_ref: refundResult.data.vnp_TxnRef,
                                vnp_command: refundResult.data.vnp_Command,
                                vnp_transaction_no: refundResult.data.vnp_TransactionNo
                            } 
                        }
                    },
                }
            }
        })
        return updateOrderRefund
    } catch (error) {
        throw error;
    }
}
export default {
    createVNPayUrlForOrder,
    refundVNPayTransaction
}