/*
  Warnings:

  - The values [admin,customer,manager,support] on the enum `e_role_code` will be removed. If these variants are still used in the database, this will fail.

*/
-- AlterEnum
BEGIN;
CREATE TYPE "e_role_code_new" AS ENUM ('GUEST', 'USER', 'SUPPORT', 'MANAGER', 'ADMIN');
ALTER TABLE "roles" ALTER COLUMN "code" TYPE "e_role_code_new" USING ("code"::text::"e_role_code_new");
ALTER TYPE "e_role_code" RENAME TO "e_role_code_old";
ALTER TYPE "e_role_code_new" RENAME TO "e_role_code";
DROP TYPE "e_role_code_old";
COMMIT;

-- AlterTable
ALTER TABLE "users" ALTER COLUMN "status" SET DEFAULT 'active';
