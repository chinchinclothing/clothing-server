import { Request } from 'express';
// import User from '../database/model/User';
// import Keystore from '../database/model/Keystore';
import ApiKey from '../interfaces/apiKey.interfaces';
import {users as PrismaUser, e_role_code as PrismaRoleCode, keystore as Keystore} from '@prisma/client';


declare interface PublicRequest extends Request {
  apiKey: ApiKey;
}

declare interface RoleRequest extends PublicRequest {
  accessRoleCodes: string[];
}

declare interface User extends PrismaUser {
  roles?: PrismaRoleCode[]|null;
}

declare interface ProtectedRequest extends RoleRequest {
  user: User;
  accessToken: string;
  keystore: Keystore;
}

declare interface Tokens {
  accessToken: string;
  refreshToken: string;
}




