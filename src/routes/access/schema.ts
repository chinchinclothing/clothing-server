import Joi from 'joi';
import { JoiAuthBearer } from '../../helpers/validator';

export default {
    credential: Joi.object().keys({
        email: Joi.string().required().email(),
        password: Joi.string().required().min(6),
    }),

    refreshToken: Joi.object().keys({
        refreshToken: Joi.string().required().min(1),
    }),

    auth: Joi.object()
        .keys({
        authorization: JoiAuthBearer().required(),
        })
        .unknown(true),
    
    login: Joi.object().keys({
        username: Joi.string().required(),
        password: Joi.string().required().min(6),
    }),
    
    signup: Joi.object().keys({
        email: Joi.string().required().email(),
        username: Joi.string().required(),
        full_name: Joi.string().allow(''),
        dob: Joi.date().allow(''),
        address: Joi.string().allow(''),
        password: Joi.string().required()
            .regex(/^(?=.*[A-Z])(?=.*[!@#$%^&*])(?=.*[0-9]).*$/)
            .required()
            .max(255)
            .min(8)
            .messages({
                'string.pattern.base': "{#label} Your password must contain at least one uppercase letter, one special character (e.g., !, @, #, $), and one number. ",
                'string.base': '{#label} should be a type of "text"',
                'string.empty': '{#label} cannot be an empty field',
                'string.min': '{#label} should have a minimum length of {#limit}',
                'any.required': '{#label} is a required field',
            }),
  }),
};


