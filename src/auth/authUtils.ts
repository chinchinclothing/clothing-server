import { Tokens } from 'app-request';
import { AuthFailureError, InternalError } from '../core/ApiError';
import JWT, { JwtPayload } from '../core/JWT';
// import { Types } from 'mongoose';
import User from '../interfaces/users';
import { tokenInfo } from '../config';
import { saltRounds } from '../config';
import bcrypt from 'bcrypt';

export async function hash (password:string){
  // Hash password before save to db
  const salt = await bcrypt.genSalt(saltRounds);
  // Mã hóa mật khẩu sử dụng bcrypt
  const hashedPassword = await bcrypt.hash(password, salt);

  return hashedPassword;
}

export async function compare (password:string, passwordHash:string) {
  // compare password
  const passwordMatch = await bcrypt.compare(password, passwordHash);
  if (passwordMatch) {
      return passwordMatch;
  }
}

//jwt
export const createTokens = async (
  user: User,
  accessTokenKey: string,
  refreshTokenKey: string,
): Promise<Tokens> => {
  const accessToken = await JWT.encode(
    new JwtPayload(
      tokenInfo.issuer,
      tokenInfo.audience,
      user.id.toString(),
      accessTokenKey,
      tokenInfo.accessTokenValidity,
    ),
  );

  if (!accessToken) throw new InternalError();

  const refreshToken = await JWT.encode(
    new JwtPayload(
      tokenInfo.issuer,
      tokenInfo.audience,
      user.id.toString(),
      refreshTokenKey,
      tokenInfo.refreshTokenValidity,
    ),
  );

  if (!refreshToken) throw new InternalError();

  return {
    accessToken: accessToken,
    refreshToken: refreshToken,
  } as Tokens;
};

export const getAccessToken = (authorization?: string) => {
  if (!authorization) throw new AuthFailureError('Invalid Authorization');
  if (!authorization.startsWith('Bearer '))
    throw new AuthFailureError('Invalid Authorization');
  return authorization.split(' ')[1];
};

export const validateTokenData = (payload: JwtPayload): boolean => {
  if (
    !payload ||
    !payload.iss ||
    !payload.sub ||
    !payload.aud ||
    !payload.prm ||
    payload.iss !== tokenInfo.issuer ||
    payload.aud !== tokenInfo.audience ||
    !payload.sub
  )
    throw new AuthFailureError('Invalid Access Token');
  return true;
};
