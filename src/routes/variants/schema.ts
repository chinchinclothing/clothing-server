import Joi from "joi";
import {Prisma} from "@prisma/client";

export default {
    variantCreate: Joi.object().keys({
        product_id: Joi.number().required(),
        color_id: Joi.number().required(),
        size_id: Joi.number().required(),
    }),
    variantId: Joi.object().keys({
        id: Joi.number().required()
    }),
    variantUpdate: Joi.object().keys({
        product_id: Joi.number(),
        color_id: Joi.number(),
        size_id: Joi.number(),
    }),

}