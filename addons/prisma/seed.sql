INSERT INTO users (id, username, email, password)
VALUES 
(1, 'admin', 'lvd.admin@gmail.com', '$2b$10$KpeisZoUwTSZTrXaPFH2BOaScVIkd7eWXm.pfAwYYcNN5P9PfMUgC'),
(2, 'support', 'lvd.support@gmail.com', '$2b$10$KpeisZoUwTSZTrXaPFH2BOaScVIkd7eWXm.pfAwYYcNN5P9PfMUgC');


INSERT INTO roles (id, code, des)
VALUES
(1, 'admin', 'full access'),
(2, 'support', 'limited access');

INSERT INTO user_role(user_id, role_id)
VALUES
(1, 1),
(2, 2);
(2, 3);

-- change user user
update users set username = 'support' where id = 2;

select * from users;
select * from roles;
select * from user_role;



