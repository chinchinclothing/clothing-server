import { PrismaClient } from '@prisma/client'
const prisma = new PrismaClient()

async function main() {

    /**
     * create roles
     */
    const admin = await prisma.roles.upsert({
        where: { code: 'ADMIN' },
        update: {},
        create: {
            code: 'ADMIN',
            des: 'full access',
            status: true,
        },
    })

    const support = await prisma.roles.upsert({
        where: { code: 'SUPPORT' },
        update: {},
        create: {
            code: 'SUPPORT',
            des: 'support access',
            status: true,
        },
    })

    /**
     * create users
     */
    const userAdmin = await prisma.users.upsert({
        where: { username: 'admin'},
        update: {},
        create: {
            email: "lvd.admin@gmail.com",
            username: "admin",
            full_name: "Le Van Dat",
            dob: "2023-03-23",
            address: "vvv",
            password:"$2b$10$hs7fEKdbQvRCiEWubVkYoubXWisbb73i1gV.bwbF4UVTtu6q0Y3tS"
        }
    })

    const userSupport = await prisma.users.upsert({
        where: { username: 'support'},
        update: {},
        create: {
            email: "lvd.support@gmail.com",
            username: "support",
            full_name: "Le Van Dat",
            dob: "2023-03-23",
            address: "vvv",
            password:"$2b$10$hs7fEKdbQvRCiEWubVkYoubXWisbb73i1gV.bwbF4UVTtu6q0Y3tS"
        }
    })

    /**
     * create user roles
     */
    const userRoleAdmin = await prisma.user_role.upsert({
        where: { user_id: userAdmin.id , role_id: admin.id},
        update: {},
        create: {
            user_id: userAdmin.id,
            role_id: admin.id,
            status: true
        }
    })

    const userRoleSupport = await prisma.user_role.upsert({
        where: { user_id: userSupport.id , role_id: support.id},
        update: {},
        create: {
            user_id: userSupport.id,
            role_id: support.id,
            status: true
        }
    })
    
    
main()
  .then(async () => {
    await prisma.$disconnect()
  })
  .catch(async (e) => {
    console.error(e)
    await prisma.$disconnect()
    process.exit(1)
  })