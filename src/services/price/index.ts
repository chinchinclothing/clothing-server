import {Prisma} from '@prisma/client'
import prisma from '../../db/prisma/connection';
import _ from 'lodash';

async function updateProductRange(id:number, min: number|null, max: number|null){
    await prisma.products.update({
        where: {id},
        data: {
            min_price: min, 
            max_price: max
        }
    })
}
async function create(params: Prisma.pricesCreateInput){
    try {
        
        const price = await prisma.prices.create({
            data: params
        });

        return price;
    } catch (error) {
        throw error;
    }
}



async function findFirst(params: Prisma.pricesWhereInput){
    try {
        const price = await prisma.prices.findFirst({
            where: params
        })
        return price;
    } catch (error) {
        throw error;
    }
}

async function findByPk(id: number){
    try {
        const price = await prisma.prices.findUnique({
            where: {id}
        })
        return price;
    } catch (error) {
        throw error;
    }
}

async function update(id: number, params: Prisma.pricesUpdateInput){
    try {
        const price = await prisma.prices.update({
            where: {id},
            data: params
        })
        return price;
    } catch (error) {
        throw error;
    }
}

export default {
    create,
    findFirst,
    findByPk,
    update
}