import { SuccessMsgResponse } from './../../../core/ApiResponse';
import { Router } from "express";
import asyncHandler from "../../../helpers/asyncHandler";
import authorization from "../../../auth/authorization";
import authentication from "../../../auth/authentication";
import accessRoles from "../../../helpers/accessRoles";
import { e_role_code as RoleCode } from "@prisma/client";
import validator from "../../../helpers/validator";
import cartService from '../../../services/cart';
import cartItemsServices from '../../../services/cart/cartItems';
import deliveryServices from '../../../services/deliveries';
import voucherServices from '../../../services/vouchers';
import productServices from '../../../services/products'
import shipmentServices from '../../../services/shipment';
import {SuccessResponse} from "../../../core/ApiResponse";
import { BadRequestError, InternalError } from "../../../core/ApiError";
import { ValidationSource } from "../../../helpers/validator";
import { ProtectedRequest } from "app-request";
import schema from "../schema";
import cart from "..";
import _ from "lodash";
//================================================
const router  =  Router();
//================================================
router.use(
    authentication,
    accessRoles(RoleCode.USER),
    authorization,
);
//================================================
/**
 * 
 * get pre-order information
 * 
 */
router.post(
    '/pre-order',
    validator(schema.createCheckout),
    asyncHandler(
        async (req: ProtectedRequest, res)=>{
            //check address
            const address = await deliveryServices.getDeliveryLocationsById(req.body.shipping_address_id);
            if(!address){
                throw new BadRequestError("Address not found");
            }
            
            if(address.user_id !== req.user.id){
                throw new BadRequestError("Address not belong to your account");
            }

            //check payment method
            const paymentMethod = req.body.payment_method;
            if(!paymentMethod){
                throw new BadRequestError("Payment method not found");
            }

            //check cart items
            const item_ids = _.map(req.body.cart_items, (item)=>{return _.parseInt(item.item_id)});

            const items = await cartItemsServices.findCartItemDetailsByManyIds(item_ids);

            if(items.length !== item_ids.length){
                throw new BadRequestError("Items not found in your cart" );
            }

            //  check change price
            const itemsChange = items.some((item)=>{
                    if(item.variants?.prices?.price !== item.price){
                        throw new BadRequestError(`Price has been changed, try again`);
                    }; 
                    if(item.quantity > item.variants.stock){
                        throw new BadRequestError("Item out of stock");
                    }
                }
            )

            
            //calculate total
            const merchandiseTotal = _.sumBy(items, (item)=>{return item.price * item.quantity});
            
            //check coupon
            const coupon = await voucherServices.findUserCouponActiveByCode(req.user.id, req.body.coupon_code);
            if(!coupon){
                throw new BadRequestError("Coupon not found");
            }

            let discount = 0;
            if (coupon.discount_type === 'amount') {
                discount = coupon?.discount_amount?coupon?.discount_amount.toNumber():0;
            }
            
            if (coupon.discount_type === 'percentage') {
                const percentage = coupon?.discount_percentage?coupon?.discount_percentage.toNumber():0;
                discount = merchandiseTotal * percentage / 100;
            }
            
            //calculate shipping cost
                //calculate total weight
            const totalQuantity = _.sumBy(items, (item)=>{return item.quantity});
            const totalWeight = _.sumBy(items, (item)=>{return 0.5 * totalQuantity});
                //get master address
            const masterAddress = await deliveryServices.getMasterAddress();
            console.log("masterAddress",masterAddress)
                //calculate shipping cost
            const shippingCost = await shipmentServices.calculateShippingCost(totalWeight,masterAddress?.state_location_id as number, address.state_location_id);

            // return response
            return new SuccessResponse(
                "Success",
                {
                    shipping_address:address,
                    payment_method: {
                        method: paymentMethod
                    },
                    order_items: items,
                    ...(coupon&&{coupon: coupon}),
                    merchandise_total:merchandiseTotal,
                    shipping_cost: shippingCost,
                    ...(coupon&&{discount: discount}),
                    total_payment: merchandiseTotal + shippingCost - discount,
                    notes: req.body.notes
                }
            ).send(res);

        }
    )
)

/**
 * 
 * get checkout information
 * 
 */
router.post(    
    '/',
    validator(schema.createCheckoutItem),
    asyncHandler(
        async (req: ProtectedRequest, res)=>{
            // check item exist
            console.log("item_ids",req.body.item_ids)
            const items = await cartItemsServices.findCartItemDetailsByManyIds(_.map(req.body.item_ids, _.parseInt));
            if(!items){
                throw new BadRequestError("Item not found");
            }
            //  check change price
            const itemsChange = items.some((item)=>{
                    if(item.variants?.prices?.price !== item.price){
                        throw new BadRequestError(`Price has been changed, try again`);
                    }; 
                    if(item.quantity > item.variants.stock){
                        throw new BadRequestError("Item out of stock");
                    }
                }
            )

            // return response
            return new SuccessResponse(
                "Success",
                {
                    items,
                }
            ).send(res);
        }
    )
)


/**
 * 
 * confirm checkout
 * 
 */
router.get(
    '/'
)

export default router