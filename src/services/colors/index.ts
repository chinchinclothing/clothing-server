import { Prisma, colors as Colors } from '@prisma/client';
import prisma from '../../db/prisma/connection';

async function create (color: Prisma.colorsCreateManyInput): Promise<Colors|null> {
    return await prisma.colors.create({
        data: color
    });
}

async function findColor(color: Colors): Promise<Colors|null> {
    try {
        return await prisma.colors.findFirst({
            where: color
        });
    } catch (error) {
        throw error;
    }
}

async function findByPk(id:number): Promise<Colors|null> {
    try {
        return await prisma.colors.findUnique(
            { where: { id: id } }
        );
    } catch (error) {
        throw error;
    }
}

async function findAll(params: Prisma.colorsWhereInput): Promise<Colors[]> {
    try {
        let where: Prisma.colorsWhereInput = {};
        if (params.color) {
            where.color = {
                contains: String(params.color),
                mode: 'insensitive'
            };
        }
        
        return await prisma.colors.findMany(
            {
                where: where
            }
        );
    } catch (error) {
        throw error;
    }
}

async function update(id: number, color: Prisma.colorsUpdateInput): Promise<Colors|null> {
    try {
        return await prisma.colors.update({
            where: { id: id },
            data: color
        });
    } catch (error) {
        throw error;
    }
}

export default {
    create,
    findColor,
    findByPk,
    findAll,
    update
}