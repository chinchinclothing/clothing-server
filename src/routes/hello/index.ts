import express from 'express';
import { SuccessResponse } from '../../core/ApiResponse';
import { PublicRequest } from 'app-request';
import { BadRequestError } from '../../core/ApiError';
// import validator from '../../helpers/validator';
// import schema from './schema';
import asyncHandler from '../../helpers/asyncHandler';
import _ from 'lodash';
// import authentication from '../../auth/authentication';
import * as helloCtrl from '../../controllers/hello.controller';

const router = express.Router();

/*-------------------------------------------------------------------------*/
// router.use(authentication);
/*-------------------------------------------------------------------------*/

router.get(
  '/:name',
  asyncHandler(helloCtrl.sayHello),
);

export default router;
