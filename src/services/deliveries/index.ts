import {Prisma} from '@prisma/client'
import prisma from '../../db/prisma/connection';
import _ from 'lodash';

async function createContact(data: Prisma.delivery_contactCreateInput){
    try {
        const delivery_contact = await prisma.delivery_contact.create({
            data,
            include:{
                state_location:true,
                city_location:true,
                district_location:true
            }
        });
        return delivery_contact;
    } catch (error) {
        throw error;
    }
}

async function checkDeliveryIsValid(state_location_id: number, city_location_id: number, district_location_id: number){
    try {

        const location = await prisma.locations.findFirst({
            where:{
                id: district_location_id,
                locations: {
                    id: city_location_id,
                    locations: {
                        id: state_location_id,
                    }
                }

            }
        });
        return location;
    } catch (error) {
        throw error;
    }
}

async function setDefaultDeliveryLocationIsFalse(user_id: number){
    try {
        const location = await prisma.delivery_contact.updateMany({
            where:{
                user_id,
            },
            data:{
                is_default: false
            }
        });
        return location;
    } catch (error) {
        throw error;
    }
}

async function getDeliveryLocationsByUserId(user_id: number){
    try {
        const location = await prisma.delivery_contact.findMany({
            where:{
                user_id,
            },
            include:{
                state_location:true,
                city_location:true,
                district_location:true
            }
        });
        return location;
    } catch (error) {
        throw error;
    }
}

async function getDeliveryLocationsByFilter(filter: Prisma.delivery_contactWhereInput){
    try {
        const location = await prisma.delivery_contact.findFirst({
            where:filter,
            include:{
                state_location:true,
                city_location:true,
                district_location:true
            }
        })
        return location;
    }
    catch(error){
        throw error;
    }
}

async function updateDeliveryContact(id: number, data: Prisma.delivery_contactUpdateInput){
    try {
        console.log("data:",data);
        const location = await prisma.delivery_contact.update({
            where:{
                id
            },
            include:{
                state_location:true,
                city_location:true,
                district_location:true
            },
            data
        });
        return location;
    } catch (error) {
        throw error;
    }
}

async function getDeliveryLocationsById(id: number){
    try {
        const location = await prisma.delivery_contact.findUnique({
            where:{
                id
            },
            include:{
                state_location:true,
                city_location:true,
                district_location:true
            }
        });
        return location;
    }
    catch(error){
        throw error;
    }
}

async function removeDeliveryContact(id: number){
    try {
        const location = await prisma.delivery_contact.delete({
            where:{
                id
            },
            include:{
                state_location:true,
                city_location:true,
                district_location:true
            }
        });
        return location;
    }
    catch(error){
        throw error;
    }
}

async function getDefaultDeliveryLocationByUserId(user_id: number){
    try {
        const location = await prisma.delivery_contact.findFirst({
            where:{
                user_id,
                is_default: true
            },
            include:{
                state_location:true,
                city_location:true,
                district_location:true
            }
        });
        return location;
    }
    catch(error){
        throw error;
    }
}

async function addMasterAddress(data: Prisma.master_addrCreateInput){
    try {
        const location = await prisma.master_addr.create({
            data
        });
        return location;
    } catch (error) {
        throw error;
    }
}

async function getMasterAddress(){
    try {
        const location = await prisma.master_addr.findFirst(
            {
                where: {status: 'active'}   
            }
        );
        return location;
    } catch (error) {
        throw error;
    }
}

async function updateMasterAddress(id: number, data: Prisma.master_addrUpdateInput){
    try {
        const location = await prisma.master_addr.update({
            where:{
                id
            },
            data
        });
        return location;
    } catch (error) {
        throw error;
    }
}
export default {
    createContact,
    checkDeliveryIsValid,
    setDefaultDeliveryLocationIsFalse,
    getDeliveryLocationsByUserId,
    getDeliveryLocationsByFilter,
    getDeliveryLocationsById,
    updateDeliveryContact,
    removeDeliveryContact,
    getDefaultDeliveryLocationByUserId,
    addMasterAddress,
    getMasterAddress,
    updateMasterAddress
}