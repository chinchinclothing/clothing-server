/*
  Warnings:

  - The values [pre-order] on the enum `e_product_status` will be removed. If these variants are still used in the database, this will fail.

*/
-- AlterEnum
BEGIN;
CREATE TYPE "e_product_status_new" AS ENUM ('active', 'inactive', 'out_of_stock', 'pre_order', 'archived', 'on_sale', 'backorder');
ALTER TABLE "products" ALTER COLUMN "status" TYPE "e_product_status_new" USING ("status"::text::"e_product_status_new");
ALTER TYPE "e_product_status" RENAME TO "e_product_status_old";
ALTER TYPE "e_product_status_new" RENAME TO "e_product_status";
DROP TYPE "e_product_status_old";
COMMIT;
