import { products } from '@prisma/client';
import {Prisma} from '@prisma/client'
import prisma from '../../db/prisma/connection';
import _ from 'lodash';

 async function create(product:Prisma.productsCreateInput){
    try {

        const newProduct = await prisma.products.create({
            data:product
        })
        return newProduct;
    } catch (error) {
        throw error;
    }
} 

 async function findUniQueByOrParams(params: any) {
    try {
        
        const orParams = _.pick(params,['slug','sku']) as any;
        const conditions = Object.keys(orParams).map((key) => {
            return {
                [key]: orParams[key]
            }
        }) 

        const product = await prisma.products.findFirst( {
            where: {
                OR: conditions
            }
        });

        return product; 
    } catch (error) {
        throw error;
    }
}

 async function findAll(params: any) {
   try {
     let where: Prisma.productsWhereInput = {};

     if (params.title) {
       where.title = {
         contains: params.title,
         mode: "insensitive",
       };
     }

     if (params.slug) {
       where.slug = {
         contains: params.slug,
       };
     }

     if (params.sku) {
       where.sku = {
         contains: params.sku,
       };
     }

     if (params.meta_title) {
       where.meta_title = {
         contains: params.meta_title,
       };
     }

     if (params.status) {
       where.status = params.status;
     }

     if (params.gender) {
       where.gender = params.min_price;
     }

     if (params.from || params.to) {
       where.created_at = {
         gte: new Date(params.from).toISOString(),
         lte: params.to
           ? new Date(params.to).toISOString()
           : new Date().toISOString(),
       };
     }

     if (params.published === true) {
       where.published_at = {
         not: null,
       };
     }
     if (params.published === false) {
       where.published_at = null;
     }

     const totalCategories = await prisma.products.count({
       where,
     });
     const take = params.limit || 10;
     const totalPages = Math.ceil(totalCategories / take);

     if (params.page > totalPages) {
       params.page = totalPages;
     }

     const skip = (params.page - 1) * take || 0;
     const products = await prisma.products.findMany({
       where,
       take,
       skip,
     });

     return { products, totalPages, currentPage: params.page || 1 };
   } catch (error) {
     throw error;
   }
 }

 async function findByUnique(
   value: string,
   type: string = "slug"
 ): Promise<products | null> {
   try {
     let where: any = {};

     if (type === "slug") {
       where = {
         slug: value,
       };
     } else if (type === "id") {
       where = {
         id: parseInt(value),
       };
     }

     const product = await prisma.products.findUnique({
       where: where as Prisma.productsWhereUniqueInput,
       include: {
         images: {
           where: {
             default: true,
           },
         },
         videos: true,
         variants: {
           include: {
             prices: {
               include: {
                 currency: true,
               },
             },
             sizes: true,
             colors: {
               include: {
                 images: true,
               },
             },
           },
         },
       },
     });

     return product;
   } catch (error) {
     throw new Error(`Error finding product: ${error}`);
   }
 }

 async function countProducts(params: any) {
   try {
     let where: Prisma.productsWhereInput = {};
     if (params.status) {
       where.status = params.status;
     }

     if (params.created_at_min || params.created_at_max) {
       let created_at_max = new Date(params.created_at_max);
       created_at_max.setDate(created_at_max.getDate() + 1);
       where.created_at = {
         gte: new Date(params.created_at_min).toISOString(),
         lte: new Date(created_at_max).toISOString(),
       };
     }

     if (params.updated_at_min || params.updated_at_max) {
       let updated_at_max = new Date(params.updated_at_max);
       updated_at_max.setDate(updated_at_max.getDate() + 1);
       where.updated_at = {
         gte: new Date(params.updated_at_min).toISOString(),
         lte: new Date(updated_at_max).toISOString(),
       };
     }

     const count = await prisma.products.count({
       where,
     });

     return count;
   } catch (error) {
     throw error;
   }
 }

 async function update(id:number,product:Prisma.productsUpdateInput){
    try {
        const updatedProduct = await prisma.products.update({
            where:{
                id
            },
            data:product
        })
        return updatedProduct;
    } catch (error) {
        throw error;
    }
}

async function getVariantsOfProducts(productId:number){
    try {
        const variants = await prisma.variants.findMany({
            where:{
                product_id:productId
            },
            include:{
                prices:{
                    include:{
                        currency:true
                    }
                },
                sizes:true,
                colors:true
            }
        })
        return variants;
    } catch (error) {
        throw error;
    }
}

async function addProductImages(productId:number,images:Prisma.imagesCreateManyInput[]){
    try {
        const product = await prisma.products.update({
            where:{
                id:productId
            },
            data:{
                images:{
                    create:images
                }
            }
        })
        return product;
    } catch (error) {
        throw error;
    }
}

async function removeProductImages (productId:number, imageIds:number[]){
    try {
        const product = await prisma.products.update({
            where:{
                id:productId
            },
            data:{
                images:{
                    deleteMany:{
                        id:{
                            in:imageIds
                        }
                    }
                }
            }
        })
        return product;
    } catch (error) {
        throw error;
    }
}

async function addProductVideos(productId:number,videos:Prisma.videosCreateManyInput[]){
    try {
        const product = await prisma.products.update({
            where:{
                id:productId
            },
            data:{
                videos:{
                    create:videos
                }
            }
        })
        return product;
    } catch (error) {
        throw error;
    }
}

async function removeProductVideos(productId:number,videoIds:number[]){
    try {
        const product = await prisma.products.update({
            where:{
                id:productId
            },
            data:{
                videos:{
                    deleteMany:{
                        id:{
                            in:videoIds
                        }
                    }
                }
            }
        })
        return product;
    } catch (error) {
        throw error;
    }
}

async function updatePriceRange(id:number){
    try {
        const priceRange = await prisma.prices.aggregate({
            where:{
                variant_id:id
            },
            _min:{
                price:true
            },
            _max:{
                price:true
            }
        });

        await prisma.products.update({
            where:{
                id
            },
            data:{
                min_price:priceRange._min.price,
                max_price:priceRange._max.price
            }
        })
    } catch (error) {
        throw error;
    }
}

async function getProductPriceByVariantId (variantId:number){
  return await prisma.products.findFirst(
        {
            where:{
                variants:{
                    some:{
                        id:variantId
                    }
                }
            },
            include:{
                variants:{
                    where:{
                        id: variantId
                    },
                    include:{
                      prices:{
                        include:{
                          currency:true
                        }
                      }
                    }
                }
            }
        }
    )
}

async function getReviewByOrderItemId(orderItemId:number){
    try {
        const review = await prisma.reviews.findFirst({
            where:{
                order_item_id:orderItemId
            }
        })
        return review;
    }
    catch (error) {
        throw error;
    }
}

async function getReviewsByProductId(productId:number){
    try {
        const reviews = await prisma.reviews.findMany({
            where:{
                order_item:{
                    variants:{
                        product_id:productId
                    }
                }
            },
            include:{
                users:true,
                order_item:{
                    include:{
                        variants:{
                            include:{
                                colors:true,
                                sizes:true
                            }
                        },
                    }
                }
            }
        })
        return reviews;
    }
    catch (error) {
        throw error;
    }
}

interface Review {
  userId: number;
  orderItemId: number;
  rating: number;
  comment: string;
}
async function createReview(review: Review){
    try {
        const newReview = await prisma.reviews.create({
            data:{
              users:{
                connect:{
                  id:review.userId
                }
              },
              order_item:{
                connect:{
                  id:review.orderItemId
                }
              },
              rating:review.rating,
              comment:review.comment
            }
        })
        return newReview;
    } catch (error) {
        throw error;
    }
}

async function updateReview(reviewId:number,comment:string){
    try {
        const updatedReview = await prisma.reviews.update({
            where:{
                id:reviewId
            },
            data:{
                comment
            },
            include:{
                users:true,
                order_item:{
                    include:{
                        variants:{
                            include:{
                                colors:true,
                                sizes:true
                            }
                        },
                    }
                }
            }
        })
        return updatedReview;
    } catch (error) {
        throw error;
    }
}

async function getReviewById(id:number){
    try {
        const review = await prisma.reviews.findFirst({
            where:{
                id
            }
        })
        return review;
    } catch (error) {
        throw error;
    }
}

export default {
    create,
    findUniQueByOrParams,
    findAll,
    findByUnique,
    countProducts,
    update,
    getVariantsOfProducts,
    addProductImages,
    removeProductImages,
    addProductVideos,
    removeProductVideos,
    updatePriceRange,
    getProductPriceByVariantId,
    getReviewsByProductId,
    getReviewByOrderItemId,
    createReview,
    updateReview,
    getReviewById
}