import Joi from 'joi';
import {e_payment_method as EPaymentMethod, e_order_status as EOrderStatus} from '@prisma/client';

export default {
    createOrder: Joi.object().keys({
        shipping_address_id: Joi.number().required(),
        payment_method: Joi.string().valid(...Object.keys(EPaymentMethod)).required(),
        order_items: Joi.array().items(
            Joi.object().keys({
                variant_id: Joi.number().required(),
                product_name: Joi.string().required(),
                quantity: Joi.number().required(),
                price: Joi.number().required(),
                compare_at_price: Joi.number().required()
            })
        ).required(),
        coupon: Joi.object().keys({
            code: Joi.string().required(),
            id: Joi.number().required(),
            discount_amount: Joi.number().required(),
            discount_percentage: Joi.number().required(),
            discount_type: Joi.string().required(),

        }),
        merchandise_total: Joi.number().required(),
        shipping_cost: Joi.number().required(),
        discount: Joi.number().required().allow(null),
        total_payment: Joi.number().required(),
        note: Joi.string().allow(null),
    }),
    adminRetrieveListOrers: Joi.object().keys({
        created_at_min: Joi.date(),
        created_at_max: Joi.date(),
        is_pair: Joi.boolean().allow(null),
        is_expired: Joi.boolean().allow(null),
        payment_method: Joi.string().valid(...Object.keys(EPaymentMethod)),
        status: Joi.string().valid(...Object.keys(EOrderStatus)),
        page: Joi.number(),
        limit: Joi.number()
    }),
    userRetrieveListOrers: Joi.object().keys({
        created_at_min: Joi.date(),
        created_at_max: Joi.date(),
        is_pair: Joi.boolean().allow(null),
        is_expired: Joi.boolean().allow(null),
        payment_method: Joi.string().valid(...Object.keys(EPaymentMethod)),
        status: Joi.string().valid(...Object.keys(EOrderStatus)),
        page: Joi.number(),
        limit: Joi.number()
    }),
    orderID: Joi.object().keys({
        id: Joi.number().required()
    }),

    adminCancelledOrder: Joi.object().keys({
        order_id: Joi.number().required(),
        reason: Joi.string().required()
    }),

    adminUpdateOrderStatus: Joi.object().keys({
        status: Joi.string().valid(...Object.keys(EOrderStatus)).required()
    }),
    id: Joi.object().keys({
        id: Joi.number().required()
    }),
}



