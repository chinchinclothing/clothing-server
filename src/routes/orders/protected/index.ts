import {Router} from 'express';
import authorization from '../../../auth/authorization';
import authentication from '../../../auth/authentication';
import { e_role_code as RoleCode, e_order_status as EOrderStatus } from '@prisma/client';
import accessRoles from '../../../helpers/accessRoles'
import validator ,{ValidationSource} from '../../../helpers/validator';
import asyncHandler from '../../../helpers/asyncHandler';
import {ProtectedRequest, PublicRequest} from 'app-request';
import * as userServices from '../../../services/users';
import { e_voucher_target as EVoucherTarget,e_voucher_type as EVoucherType } from '@prisma/client';
import schema from '../schema';
import voucherServices from '../../../services/vouchers';
import deliveryServices from '../../../services/deliveries';
import variantServices from '../../../services/variants';
import shipmentServices from '../../../services/shipment';
import orderServices from '../../../services/orders';
import settingServices from '../../../services/setting';
import productServices from '../../../services/products';

import { SuccessResponse } from '../../../core/ApiResponse';
import { BadRequestError } from '../../../core/ApiError';
import adminRouter from './admin';
import {Prisma} from '@prisma/client';
import _ from 'lodash';
//================================================================
const userRouter = Router();
userRouter.use(
    authentication,
    accessRoles(RoleCode.USER),
    authorization,
);
//================================================================
const router = Router();
router.use('/', userRouter);
router.use('/admin', adminRouter);
//================================================================

/**
 * 
 * Retrieve list of orders
 * 
 */
userRouter.get(
    '/',
    validator(schema.adminRetrieveListOrers, ValidationSource.QUERY),
    asyncHandler(async (req: ProtectedRequest, res) => {
        const filter = {
            ...req.query,
            user_id: req.user.id,
        }
        const orders = await orderServices.adminRetrieveListOrers(filter);
        new SuccessResponse('Success', {
            orders:orders.orders,
            page:orders.page,
            totalPage:orders.totalPage,
        }).send(res);
        
    })
)

/**
 * 
 * Create order
 * 
 */
userRouter.post(
    '/',
    validator(schema.createOrder),
    asyncHandler(async (req: ProtectedRequest, res) => {
        const userId = req.user.id;
        const body = req.body;
        const note = body.note;
        const orderItems = body.order_items;
        const couponCode = body.coupon_code;
         //check address
         const address = await deliveryServices.getDeliveryLocationsById(req.body.shipping_address_id);
         console.log("address", address)
         if(!address){
             throw new BadRequestError("Address not found");
         }
         
         if(address.user_id !== req.user.id){
             throw new BadRequestError("Address not belong to your account");
         }

         //check payment method
         const paymentMethod = req.body.payment_method;
         if(!paymentMethod){
             throw new BadRequestError("Payment method not found");
         }

         //check items price and stock
         const item_ids = _.map(orderItems, (item)=>{return _.parseInt(item.variant_id)});
         const items_variant = await variantServices.findByIdsDetails(item_ids);
         console.log("items_variant",items_variant)
         if(!items_variant){
             throw new BadRequestError("Item not found");
         }

         //  check change price
         const orderItemSave = _.map(items_variant,(item)=>{
                const itemCheck = orderItems.find((orderItem: any)=>{return orderItem.variant_id === item.id});
                if(itemCheck.price !== item.prices?.price){
                    throw new BadRequestError("Price of item has been changed");
                }
                if(itemCheck.quantity > item.stock){
                    throw new BadRequestError("Item out of stock");
                }
                return {
                    ...item,
                    url_img_default: item.products?.url_img_default,
                    quantity: itemCheck.quantity,
                };
             }
         )
         console.log("orderItemSave",orderItemSave)

         
         //calculate total
         const merchandiseTotal = _.sumBy(orderItemSave, (item: any)=>{return item.prices.price * item.quantity});
         console.log("merchandiseTotal",merchandiseTotal)

         //check coupon
         const coupon = await voucherServices.findUserCouponActiveByCode(userId, couponCode);

         if(!coupon){
             throw new BadRequestError("Coupon not found");
         }

         let discount = 0;
         if (coupon.discount_type === 'amount') {
             discount = coupon?.discount_amount?coupon?.discount_amount.toNumber():0;
         }
         
         if (coupon.discount_type === 'percentage') {
             const percentage = coupon?.discount_percentage?coupon?.discount_percentage.toNumber():0;
             discount = merchandiseTotal * percentage / 100;
         }
         
         //calculate shipping cost
             //calculate total weight
         const totalQuantity = _.sumBy(orderItems, (item: any)=>{return item.quantity});
         const totalWeight = 0.5 * totalQuantity;
             //get master address
         const masterAddress = await deliveryServices.getMasterAddress();
             //calculate shipping cost
         const shippingCost = await shipmentServices.calculateShippingCost(totalWeight,masterAddress?.state_location_id as number, address.state_location_id);
        //calculate total payment
        const totalPayment = merchandiseTotal + shippingCost - discount;

         const expiredTime = await settingServices.findByKey('order_expired_time') || {value: 30};
         const expiredAt = new Date( new Date().getTime() + (expiredTime?.value as number) * 60000);
         const orderData = {
            name: address.name,
            address: address.address,
            state: address.state_location.name,
            state_location_id: address.state_location_id,
            city: address.city_location.name,
            city_location_id: address.city_location_id,
            district: address.district_location.name,
            district_location_id: address.district_location_id,
            phone_number: address.phone_number,
            payment_method: paymentMethod,
            shipping_cost: shippingCost,
            discount: discount,
            total_payment: totalPayment,
            note: note||"",
            expired_at: expiredAt,
            status: paymentMethod==='VNPAY'?'WAITING_PAYMENT':'WAITING_CONFIRMATION',
        }

        const orderItemData = orderItemSave.map((item: any)=>{
            return {
                variant_id: item.id,
                price: item.prices.price,
                compare_at_price: item.prices.compare_at_price,
                product_name: item.products?.title,
                quantity: item.quantity,
                thumbnail: item.url_img_default||null,
            } 
       })
         const order = await orderServices.create(orderData, orderItemData, coupon.code, userId,);
         if (!order){
                throw new BadRequestError("Order cannot be created");
         }

        //use coupon
        await voucherServices.updateVoucherUsageLog(coupon.id, userId, order.id);

        new SuccessResponse('Order created successfully', order).send(res);
    })
)

/**
 * 
 * Cancel order
 * 
 */

userRouter.post(
    '/cancel',
    validator(schema.adminCancelledOrder),
    asyncHandler(async (req: ProtectedRequest, res) => {
        const orderId = parseInt(req.body.order_id);
        const order = await orderServices.getOrderDetail(orderId);
        if(!order) throw new BadRequestError('Order not found');
        if(order.cancelled_orders.length > 0){
            order.cancelled_orders.map((item)=>{
                switch(item.status){
                    case 'CANCELLED':
                        throw new BadRequestError('Order has been cancelled');
                    case 'PENDING_APPROVAL':
                        return new SuccessResponse('Request cancel order success, waiting for confirmation', {
                        }).send(res);
                    case 'REJECTED':
                        throw new BadRequestError('Request cancel order has been rejected');
                    case 'APPROVED':
                        throw new BadRequestError('Request cancel order has been approved');
                    case 'REFUNDED':
                        throw new BadRequestError('Order has been refunded');
                }
            })
            console.log("order.cancelled_orders",order.cancelled_orders)
        }

        if( !order.status||order.status && !["WAITING_PAYMENT","WAITING_CONFIRMATION","WAITING_PICKUP"].includes(order.status) ) 
            throw new BadRequestError('Order can not be cancel');

        const cancelData = {
            orderId: orderId as number,
            userId: req.user.id as number,
            reason: req.body.reason as string,
        }

        const cancelOrder = await orderServices.requestOrder(cancelData);

        if(!cancelOrder) throw new BadRequestError('Cancel order fail');

        return new SuccessResponse('Request cancel order success, waiting for confirmation', {
            order:cancelOrder,
        }).send(res);
    })
)

/**
 * 
 * User confirm order received
 * 
 */
userRouter.put(
    '/:id/received',
    validator(schema.orderID, ValidationSource.PARAM),
    asyncHandler(async (req: ProtectedRequest, res) => {
        const orderId = parseInt(req.params.id);
        const order = await orderServices.getOrderDetail(orderId);
        if(!order) throw new BadRequestError('Order not found');
        if(order.user_id !== req.user.id) throw new BadRequestError('Order not found');
        if(order.status === 'CANCELLED') throw new BadRequestError('Order is cancelled');
        if(order.status === 'RETURNED') throw new BadRequestError('Order is returned');
        if(order.status === 'EXPIRED') throw new BadRequestError('Order is expired');
        if(order.status === 'WAITING_PAYMENT') throw new BadRequestError('Order is waiting payment');
        if(order.status === 'COMPLETED') throw new BadRequestError('Order is completed');
        if(order.status !== 'SHIPPING') throw new BadRequestError('Order is not delivered');
        const updateOrder = await orderServices.updateOrderStatus({
            orderId: orderId,
            userId: req.user.id,
            status: 'COMPLETED'
        });
        if(!updateOrder) throw new BadRequestError('Update order status fail');
        new SuccessResponse('Update order status success', {order:updateOrder}).send(res);
    })
)



//================================================================
export default router;


