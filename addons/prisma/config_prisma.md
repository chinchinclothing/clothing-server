* Add environment to .env file
```
DB_POSTGRE_URL=postgresql://<username>:<password>@<host>:<port>/<db_name>?schema=public
```

* If database is exist 
  * Change db url in env file to existing db url  
  * Run the script below to create schema of db
```bash
npx prisma db pull
```
* Create or update Database by schema
```
npx prisma migrate dev 
```