import { Request, Response } from 'express';
import * as helloService from '../services/hello.services.js';
import { PublicRequest } from 'app-request';
import { SuccessResponse } from '../core/ApiResponse';
import _ from 'lodash';

const createCategory = async (req: PublicRequest, res:Response) => {
    const message = {message: `Hello!`};
    return new SuccessResponse(
      'success',
      _.pick(message, ['message']),
    ).send(res);
}

export { createCategory };
