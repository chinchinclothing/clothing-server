import Joi from 'joi';
import {e_voucher_target as EVoucherTarget, e_voucher_type as EVoucherType} from '@prisma/client';

export default {
    voucher: Joi.object().keys({
        title: Joi.string().required(),
        code: Joi.string().required(),               
        image: Joi.string(),     
        quantity:Joi.number(),        
        start_at: Joi.date().timestamp(),        
        end_at: Joi.date().timestamp(),          
        discount_amount:Joi.number(),  
        discount_percentage:Joi.number(),
        min_order_value:Joi.number(),  
        current_uses:Joi.number(),     
        target_type:Joi.string().valid('public','personal'),       
        discount_type: Joi.string().valid('percentage','amount')  
    }),
    voucherUpdate: Joi.object().keys({
        title: Joi.string(),
        code: Joi.string(),               
        image: Joi.string(),     
        quantity:Joi.number(),        
        start_at: Joi.date().timestamp(),        
        end_at: Joi.date().timestamp(),          
        discount_amount:Joi.number(),  
        discount_percentage:Joi.number(),
        min_order_value:Joi.number(),  
        current_uses:Joi.number(),     
        target_type:Joi.string().valid('public','personal'),       
        discount_type: Joi.string().valid('percentage','amount')  
    }),
    voucherId: Joi.object().keys({
        id: Joi.number().required()
    }),
    voucherAdd: Joi.object().keys({
        user_ids: Joi.array().items(Joi.number().required())
    }),
    voucherFilters: Joi.object().keys({
        user_id: Joi.number(),
        title: Joi.string(),
        code: Joi.string(),               
        start_at: Joi.date().timestamp(),        
        end_at: Joi.date().timestamp(),          
        target_type:Joi.string().valid(...Object.keys(EVoucherTarget)),       
        discount_type: Joi.string().valid(...Object.keys(EVoucherType))  
    })

}

