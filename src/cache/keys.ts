export enum Key {
  CATEGORIES_LATEST = 'CATEGORIES_LATEST',
}

export enum DynamicKey {
  CATEGORIES_SIMILAR = 'CATEGORIES_SIMILAR',
  CATEGORY = 'CATEGORY',
  CATEGORY_UNIQUE = 'CATEGORY_UNIQUE',
  CATEGORY_SLUG_UNIQUE = 'CATEGORY_SLUG_UNIQUE',
  CATEGORY_ID_UNIQUE = 'CATEGORY_ID_UNIQUE',
}

export type DynamicKeyType = `${DynamicKey}_${string}`;

export function getDynamicKey(key: DynamicKey, suffix: string) {
  const dynamic: DynamicKeyType = `${key}_${suffix}`;
  return dynamic;
}
