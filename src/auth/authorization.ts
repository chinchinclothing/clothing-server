import {Router} from 'express';
import asyncHandler from '../helpers/asyncHandler';
import {ProtectedRequest} from 'app-request';
import {AuthFailureError} from '../core/ApiError';
//==============================================================================
const router = Router();
//==============================================================================

export default router.use(
    asyncHandler(async (req:ProtectedRequest, res, next) => {
        //check user role
        console.log("req.accessRoleCodes",req.accessRoleCodes)
        if (!req.user || !req.user.roles || !req.accessRoleCodes)
            throw new AuthFailureError('Permission denied');
        
        //check role
        let authorized = false;
        for (const userRole of req.user.roles) {
            for (const role of req.accessRoleCodes) {
                if (userRole === role) {
                    authorized = true;
                    break;
                }
            }
        }
        
        //if not authorized
        if (!authorized) throw new AuthFailureError('Permission denied');
        return next();

    })
);