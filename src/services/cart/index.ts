import { Prisma } from "@prisma/client";
import prisma from '../../db/prisma/connection';

async function findCartByUserId(userId:number, filter:any){
    return await prisma.carts.findFirst(
        {
            where:{
                user_id:userId,
                ...filter
            }
        }
    )
}

async function createCart (data: any){
    return await prisma.carts.create({
        data
    })
}

async function findFirst(id:number, filters:Prisma.cartsWhereInput){
    return await prisma.carts.findFirst(
        {        
            where:{
                id,
                ...filters
            }
        }
    )

}

async function addItem(id:number, data: any){
    return await prisma.carts.update(
        {
            where:{
                id
            },
            data:{
                cart_item:{
                    create:{
                        ...data
                    }
                }
            }
        }
    )
}

export default {
    createCart,
    findCartByUserId,
    findFirst,
    addItem,
}