import { Prisma, sizes as Sizes } from '@prisma/client';
import prisma from '../../db/prisma/connection';

async function create (size: Prisma.sizesCreateManyInput): Promise<Sizes|null> {
    return await prisma.sizes.create({
        data: size
    });
}

async function findSize(size: Sizes): Promise<Sizes|null> {
    try {
        return await prisma.sizes.findFirst({
            where: size
        });
    } catch (error) {
        throw error;
    }
}

async function findByPk(id:number): Promise<Sizes|null> {
    try {
        return await prisma.sizes.findUnique(
            { where: { id: id } }
        );
    } catch (error) {
        throw error;
    }
}

async function findAll(params: Prisma.sizesWhereInput): Promise<Sizes[]> {
    try {
        let where: Prisma.sizesWhereInput = {};
        if (params.size) {
            where.size = {
                contains: String(params.size),
                mode: 'insensitive'
            };
        }
        
        return await prisma.sizes.findMany(
            {
                where: where
            }
        );
    } catch (error) {
        throw error;
    }
}

async function update(id: number, size: Prisma.sizesUpdateInput): Promise<Sizes|null> {
    try {
        return await prisma.sizes.update({
            where: { id: id },
            data: size
        });
    } catch (error) {
        throw error;
    }
}

export default {
    create,
    findSize,
    findByPk,
    findAll,
    update
}