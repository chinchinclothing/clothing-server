import express, { Application } from 'express';
import apikey from '../auth/apikey';
import permission from '../helpers/permission';
import { Permission } from '../interfaces/apiKey.interfaces';
import hello from './hello';
import products from './products';
import colors from './colors';
import sizes from './sizes';
import variants from './variants';
import currency from './currency';
import collections from './collections';
import cart from './cart';
import locations from './locations';
import shipments from './shipment';
import users from './users';
import checkout from './checkout';
import vouchers from './vouchers';  
import orders from './orders';
import payment from './payment';
import reviews from './reviews';

import signup from './access/signup';
import login from './access/login';
import token from './access/token';




//=========================================================

class Routes {
    constructor(app: Application) {
        if (!app) {
            throw new Error("You must provide an instance of express");
        }
        this.initializeRoutes(app);
    }

    private initializeRoutes(app: Application) {
        /*---------------------------------------------------------*/
        // app.use(apikey);
        /*---------------------------------------------------------*/
        // app.use(permission(Permission.GENERAL));
        /*---------------------------------------------------------*/
        app.use('/api/v1/hello', hello);
        /**
         * 
         * Access routes
         * 
         */
        app.use('/api/v1/signup', signup);
        app.use('/api/v1/login', login);
        app.use('/api/v1/token', token);

        app.use('/api/v1/products', products.public);
        app.use('/api/v1/protected/products', products.protected);
        
        app.use('/api/v1/protected/colors', colors.protected);
        
        app.use('/api/v1/protected/sizes', sizes.protected);

        app.use('/api/v1/protected/currency', currency.protected);

        app.use('/api/v1/protected/collections/highlights', collections.highlight.protected)
        app.use('/api/v1/collections', collections.public);
        app.use('/api/v1/protected/collections', collections.protected);

        app.use('/api/v1/protected/carts', cart.protected);

        app.use('/api/v1/locations', locations.public);
        app.use('/api/v1/protected/locations', locations.protected);

        app.use('/api/v1/shipment', shipments.public);

        app.use('/api/v1/users', users.public);
        app.use('/api/v1/protected/users', users.protected);

        app.use('/api/v1/vouchers', vouchers.public);
        app.use('/api/v1/protected/vouchers', vouchers.protected);
        
        app.use('/api/v1/protected/checkout', checkout.protected);

        app.use('/api/v1/protected/orders', orders.protected);

        app.use('/api/v1/protected/payment', payment.protected);
        app.use('/api/v1/payment', payment.public);
        
        app.use('/api/v1/protected/reviews', reviews.protected);
    }
}

//=========================================================

export default Routes;
