import { Key } from './../../cache/keys';
import Joi from 'joi';

export default {
    addCartItem: Joi.object().keys(
        {
            variant_id:Joi.number().required(),  
            quantity:Joi.number().required()
        }
    ),
    itemId: Joi.object().keys(
        {
            id: Joi.number().required()
        }
    )
}